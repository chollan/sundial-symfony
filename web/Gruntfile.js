module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        sourceMap: true,
        outputStyle: 'expanded'
      },
      primary: {
        files: {
          "css/global.css": "sass/global.scss"
        }
      }
    },

    postcss: {//after sass compiles, process the resulting css file to include prefixes and minify the result
      options: {
        processors: [
          require('autoprefixer-core')({browsers: 'last 3 versions'}), // add vendor prefixes
          require('cssnano')() // minify the result
        ]
      },
      primary: {
        map: {
          inline: false, // save all sourcemaps as separate files...
          annotation: 'css/' // ...to the specified directory
        },
        src: 'css/*.css'
      }
    },

    watch: {
      primary: {
        options: {
          reload: false,
          spawn: false,
          interrupt: false,
          livereload: true
        },
        files: [
          'sass/**/*.scss',
          'js/parts/**/*.js'
        ],
        tasks: ['sass']
      }
    }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['sass']);
  grunt.registerTask('prod', ['sass', 'postcss']);
};