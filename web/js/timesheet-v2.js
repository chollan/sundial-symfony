var Timesheet = {
  bEnableForm:true,
  activeAjaxRequest:null,
  day:null,
  count: 0,
  text : {
    defaultSubmit: "Please Wait. We're submitting your data to Open Air.",
    loadingFromAPI: "One moment, we're requesting information from Open Air.",
    createTimesheet: "Please wait while we create your timesheet.",
    prepareRequest: "Please wait while we prepare your request.",
    pullFromToggl: "Please wait while we pull your data from Toggl"
  },
  buttons:{
    $addRow : null,
    $refreshToReset : null,
    $clearCache : null,
    $importFromToggl : null,
    $submitToOa : null,
    $submitTimesheet : null
  },
  setDay: function(){
    this.day = window.location.pathname.match(/\d{4}-\d{2}-\d{2}/)[0];
  },
  setButtons:function(){
    this.buttons.$addRow = $('.addRow');
    this.buttons.$refreshToReset = $('.refreshToReset');
    this.buttons.$clearCache = $('.clearCache');
    this.buttons.$importFromToggl = $('#importFromTogglBtn');
    this.buttons.$submitToOa = $('.submitToOa');
    this.buttons.$submitTimesheet = $('.submitTimesheet');
  },
  setActions:function(){
    var that = this;

    this.buttons.$submitTimesheet.on('click', function(e){
      e.preventDefault();
      var href = $(this).attr('href');
      bootbox.confirm("Are you sure you're ready to submit your timesheet?", function(bResult){
        if(bResult){
          window.location.href = href;
        }
      });
    })

    this.buttons.$addRow.on('click', function(e){
      e.preventDefault();
      var html = $('#form_new').data('prototype');
      var $target = $('.newTarget');
      html = html.replace(/__name__/g, that.count);
      that.count++;
      $target.before(html);
      $('.pendingText').show();
      that.enableButtons();
      that.initTypeAhead();
    });

    $('a.jumpToDay').on('click', function(){
      waitingDialog.show(that.text.loadingFromAPI);
    });

    $('#timegrid').on('submit', function(){
      waitingDialog.show(that.text.defaultSubmit);
    })

    if(this.bEnableForm) {
      $(document).on('click', '.delete-time-entry', function () {
        var that = this;
        bootbox.confirm("Are you sure you want to remove this time entry?<br/>A save will be required.", function (bResult) {
          if (bResult) {
            $(that).closest('tr').remove();
            Timesheet.enableButtons();
          }
        });
      });
    }


    $(document).on('change', '.timetype_select', function(){
      that.enableButtons();
    });

    $(document).on('change', "select[id$='_task']", function(){
      $.ajax({
        url: '/rest/lastTask',
        method: 'post',
        dataType: 'json',
        data: {task:$(this).val()}
      });
      that.enableButtons();
    });

    $(document).on('change', '.time-entry', function(){
      var total = 0.0;
      $.each($('.time-entry'), function(idx, el){
        total += parseFloat($(el).val());
      });
      $('#totalTimeForDay').text(total);
      that.enableButtons();
    });

    $(document).on('change', '.description-entry', function(){
      that.enableButtons();
    });

    this.buttons.$refreshToReset.on('click', function(){
      waitingDialog.show(that.text.loadingFromAPI);
      location.reload();
    });

    this.buttons.$clearCache.on('click', function(){
      waitingDialog.show(that.text.loadingFromAPI);
    });

    $(document).on('change', "select[id$='_project']", function(){
      var selectionVal = $(this).val();
      var url = '/rest/tasks/'+selectionVal;
      that.enableButtons();
      var el = this;
      $.ajax({
        url: url,
        success: function(data){
          var selector = $(el).closest('td').next().find('select');
          selector.removeClass('typeAhead');
          selector.removeProp('disabled');
          selector.selectpicker('destroy');
          selector.addClass('selectpicker');
          selector.empty();
          for(key in data){
            var text = '';
            if(!data[key].task.is_phase){
              text = data[key].task.id_number+'. '+data[key].task.name;
              var objParent = data[key].task.parent;
              if(objParent){
                text += ' ['+objParent.name+']';
              }
              selector.append($('<option />', {value:data[key].task.id, text:text, selected:data[key].selected}))
            }

          }
          that.initTypeAhead();
        }
      });
    });

    $('.date').datepicker({
      orientation: 'bottom',
      todayBtn: true,
      todayHighlight: true,
      autoclose: true
    }).on('changeDate', function(e){
      waitingDialog.show(that.text.loadingFromAPI);
      var dateObj = new Date(e.date);
      var month = dateObj.getUTCMonth() + 1; //months from 1-12
      if(month < 10)month = "0"+month;
      var day = dateObj.getUTCDate();
      if(day < 10)day = "0"+day;
      var year = dateObj.getUTCFullYear();
      newdate = year + "-" + month + "-" + day;
      location.href="/timesheet/"+newdate;
    });

    this.buttons.$importFromToggl.on('click', function(){
      if(that.bEnableForm) {
        waitingDialog.show(that.text.pullFromToggl);
        Timesheet.activeAjaxRequest = $.ajax({
          url: '/rest/fetchTogglTime/' + that.day,
          method: 'get',
          dataType: 'json',
          data: {date: Timesheet.date},
          statusCode: {
            200: function (result, code, xhr) {
              $('.importTarget').html(result);
              that.initTypeAhead();
            },
            204: function (a, b, c) {
              bootbox.alert("There is no new data to import from toggl.", function () {
              });
            },
            401: function () {
              bootbox.dialog({
                message: "You are not yet authenticated to query against Togg.  Please login.",
                buttons: {
                  dismiss: {
                    label: 'Login',
                    className: "btn-primary",
                    callback: function () {
                      window.location.href = "/timesheet/togglsettings"
                    }
                  },
                  login: {
                    label: 'Later',
                    className: "btn",
                    callback: function () {
                    }
                  }
                }
              });
            }
          }
        }).always(function () {
          Timesheet.activeAjaxRequest = null;
          waitingDialog.hide();
        });
      }else{
        bootbox.alert("This day belongs to a timesheet that already has been submitted.  Please consult Open Air to change time associated with this day.", function(){});
      }
    });
  },
  enableButtons:function(){
    this.buttons.$submitToOa.prop('disabled', false);
    this.buttons.$refreshToReset.prop('disabled', false);
  },
  initTypeAhead: function(){
    $('.selectpicker:not(.typeAhead)').selectpicker({
      liveSearch : true,
      selectOnTab : true
    }).on('show.bs.select',function(e){
      $('.bs-searchbox:visible input').focus();
    }).addClass('typeAhead'); // Adding Class so new rows dont reinstantiate old ones
  },
  init: function(bEnableForm){
    this.bEnableForm = bEnableForm;
    if(!this.bEnableForm){
      $('select, submit, input, textarea, button',$('form#timegrid')).prop('disabled', 'disabled');
    }
    this.setDay();
    this.setButtons();
    this.setActions();
    if(this.bEnableForm) {
      this.initTypeAhead();
    }
    $("span[rel=tooltip]").tooltip({
      html: true,
      placement: 'right'
    });
  }
}