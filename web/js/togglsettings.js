var TogglSettings = {
	activeAjaxRequest:null,
	$form: null,
	init:function(){
		this.$form = $('form.settings');
		var that = this;
		this.$form.on('change', '.clientid', function(){
			var $tableRow = $(this).parents('tr');
			var $origionalDropdown = $('.task', $tableRow);
			$origionalDropdown.html('<img src="/application/assets/img/loading.gif">');
			var ProjectID = $origionalDropdown.data('togglProjectId');

			var IDs = this.value.split(':');
			if(that.activeAjaxRequest){
				that.activeAjaxRequest.abort();
			}
			that.activeAjaxRequest = $.ajax({
				url: '/tools/loadTaskcodes',
				method: 'post',
				data: {customerid:IDs[0], projectid:IDs[1]},
				dataType: 'json'
			}).done((function(data){
				if(data.status == true){
					$origionalDropdown.empty();
					var $newDiv = $('<div />', {class:'form-group'}).appendTo($origionalDropdown);
					var $newSelect = $('<select />',{
						name: 'data['+ProjectID+'][projecttask]',
						class: 'form-control task'
					}).appendTo($newDiv);

					$.each(data.result, function(value,text) {
						$newSelect.append($("<option />", {
							value: value,
							text: text
						}));
					});
					$('.task', $tableRow).html($origionalDropdown.html());
				}else{
					location.href="/logout"
				}
				that.activeAjaxRequest = null;
			}).bind(this));
		});
	}
}

$(function(){
	TogglSettings.init();
});