var Report = {
	reportID:null,
	activeAjaxRequest: null,
	init: function(intReportID, bIsRunning){
		this.reportID = intReportID;
		if(bIsRunning && this.reportID){
			waitingDialog.show("Please wait... This report is currently running.  The page will automatically reload once the report is complete.");
			intervalID = setInterval(this.checkIsRunning,3000);
		}
	},
	checkIsRunning: function(){
		var that = this;
		if(!this.activeAjaxRequest){
			console.log(Report.reportID)
			this.activeAjaxRequest = $.ajax({
				url: '/tools/isRepoerRunning',
				method: 'post',
				data: {reportid:Report.reportID},
				dataType: 'json'
			}).done(function(data){
				if(data.status){
					if(data.isRunning){
						//do nothing, the report is still running
					}else{
						//reload the page
						location.reload();
					}
				}else{
					//do nothing, we'll check again in 3 seconds
				}
			}).always(function(){
				that.activeAjaxRequest = null;
			});
		}
	}
}