$(function(){
	$('select#customer').on('change', function(e){
		var customerID = this.value
		waitingDialog.show('Please wait while we load Open Air data.')
		$.ajax({
			url: '/tools/loadCustomerProjects',
			method: 'post',
			data: {customerid:customerID},
			dataType: 'json'
		}).done((function(data){
			if(data.status == true){
				$('.project_dropdown select').remove()
				var $newSelect = $('<select />',{
					name: 'project',
					class: 'form-control project'
				}).appendTo($('.project_dropdown .form-group'));

				$.each(data.result, function(value,text) {
					$newSelect.append($("<option />", {
						value: value,
						text: text
					}));
				});
				$('.project_dropdown').show();
				waitingDialog.hide()
			}else{
				waitingDialog.hide()
				$('select#customer').val(0);
				alert("There was a problem fetching data from Open Air.");
			}
			console.log(this, data)
		}).bind(this));
	})

	$('body').on('change', 'select.project', function(e){
		console.log(e)
	})

	$('form').on('submit', function(){
		waitingDialog.show('Please wait while we initialize this report with rate information and other pertinent data.')
	})
});