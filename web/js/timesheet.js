var Timesheet = {
	activeAjaxRequest: null,
	$datePicker: null,
	date: null,
	timesheetID: null,
	timerRunning: false,
	text: {
		defaultSubmit: "Please Wait. We're submitting your data to Open Air.",
		loadingFromAPI: "One moment, we're requesting information from Open Air.",
		createTimesheet: "Please wait while we create your timesheet.",
		prepareRequest: "Please wait while we prepare your request.", 
		pullFromToggl: "Please wait while we pull your data from Toggl"
	}, 
	updateTimesheetTimeTotal: function(){
		var totalTime = 0;
		var $timeInputs = $(this.timeInputs);
		var $totalTimeForDay = $('#totalTimeForDay');
		var dayPercentage = 0;
		$timeInputs.each((function(key, el){
			var flNumber = parseFloat($(el).val());
			
			if(!isNaN(flNumber)){
				totalTime += flNumber;
			}
			if(key+1 == $timeInputs.length){
				//this is the last iteration of the loop, let's do some stuff
				$totalTimeForDay.val(totalTime);
				dayPercentage = Math.round((totalTime/8) * 100);
				$('.todayCompeletedHours', this.$dayProgressContainer).text(totalTime);
				$('.todayCompeletedPercent', this.$dayProgressContainer).text(dayPercentage);
				if(dayPercentage < 100){
					$('.progress-bar', this.$dayProgressContainer).addClass('progress-bar-info');
					$('.progress-bar', this.$dayProgressContainer).removeClass('progress-bar-success');
					$('.progress-bar', this.$dayProgressContainer).css('width', dayPercentage+'%');
				}else if(dayPercentage >= 100){
					$('.progress-bar', this.$dayProgressContainer).removeClass('progress-bar-info');
					$('.progress-bar', this.$dayProgressContainer).addClass('progress-bar-success');
					$('.progress-bar', this.$dayProgressContainer).css('width', '100%');
				} 
				$('.progress-bar', this.$dayProgressContainer).attr('aria-valuenow', dayPercentage);
				
				if(this.$dayProgressContainer.is(":hidden")){
					this.$dayProgressContainer.show();
				}
			}
		}).bind(this));
	},
	buttons:{
		$addRow: null,
		$submitToOa:null,
		$clearCache:null,
		$importFromToggl:null,
		$acceptImportedTime:null,
		$submitTimesheet:null,
		$weekViewRedirect: null,
		$timeInputs: null,
		init: function(){
			this.$importFromToggl.on('click', function(){
				waitingDialog.show(Timesheet.text.pullFromToggl);
				Timesheet.activeAjaxRequest = $.ajax({
					url: '/tools/fetchTogglTime',
					method: 'post',
					dataType: 'json',
					data: {date:Timesheet.date}
				}).done((function(result){
					//console.log(result)
					if(result.status == true){
						if(result.data.length > 0){
							var floTotals = 0
							$.each(result.data, function(key, obj){
								floTotals += obj.decimal_hours;
								if(result.data.length == key+1){
									Timesheet.timerRunning = result.timer_running;
									Timesheet.$importTarget.html(Timesheet.views.templates.importTable({
										records: result.data, 
										date:Timesheet.date, 
										total: floTotals,
										timesheetid: result.timesheetid
									}));
									Timesheet.$importedDataForm = $('form#importedDataForm');
									
								}
							})
							$(document).trigger('FormReady');
							Timesheet.initTypeAhead();
						}else{
							bootbox.alert("There is no new data to import from toggl.", function(){});
						}
					}else{
						if(result.needAuth){
							location.href="/timeimport/toggl/authenticate";
						}
					}
					Timesheet.activeAjaxRequest = null;
					waitingDialog.hide();
				}).bind(this));
			});

			this.$submitToOa.prop('disabled', 'disabled');

			this.$clearCache.on('click', (function(e){
				waitingDialog.show(Timesheet.text.loadingFromAPI);
				location.href="/timeimport/toggl/clearCache/"+Timesheet.date;
			}).bind(this))

			this.$addRow.on('click', function(e){
				e.preventDefault();
				var Count = Timesheet.$table.find('tr').length;
				Timesheet.$table.find('tbody tr:last').before(Timesheet.views.templates.timesheetRow({count:Count}));
				Timesheet.initTypeAhead();
			})

			this.$submitToOa.on('click', (function(e){
				//if(!Timesheet.timerRunning){
					waitingDialog.show(Timesheet.text.defaultSubmit);
					Timesheet.$form.submit();
					$('body').removeClass('unsaved-changes');
				//}else{
				//	alert('You currently have a timer running.  Time can not be imported if there\s a timer running');
				//}
				
			}).bind(this));

			this.$submitTimesheet.on('click', function(e){
				/*bootbox.dialog({
					message: "Are you sure you're ready to submit this timesheet for approval?  You WILL NOT recieve a warning if you have below 40 hours!!",
					title: "Confirm Timesheet Submission",
					buttons: {
						default: {
							label: "Yes",
							className: "btn-default",
							callback: function() {
								waitingDialog.show(Timesheet.text.defaultSubmit);
								location.href="/timeimport/toggl/submit/"+Timesheet.timesheetID;
							}
						},
						main: {
							label: "No",
							className: "btn-primary",
							callback: function() {
								//we don't need to do anything here 
							}
						}
					}
				});*/
				bootbox.dialog({
					message: "PLEASE CONFIRM YOUR TIMESHEET IS CORRECT BEFORE YOU SUBMIT!  Use the button on the bottom of this dialogue to navigate to your timesheet to confirm it is correct.  If you don't feel you need to confirm it's correct, click 'I Acknowledge' and submit on the next dialogue window",
					title: "WARNING",
					buttons: {
						default: {
							label: "I acknowledge",
							className: "btn-warning",
							callback: function() {
				
								bootbox.dialog({
									message: "Are you sure you're ready to submit this timesheet for approval?  You WILL NOT recieve a warning if you have below 40 hours!!",
									title: "Confirm Timesheet Submission",
									buttons: {
										default: {
											label: "Yes",
											className: "btn-default",
											callback: function() {
												waitingDialog.show(Timesheet.text.defaultSubmit);
												location.href="/timeimport/toggl/submit/"+Timesheet.timesheetID;
											}
										},
										main: {
											label: "No",
											className: "btn-primary",
											callback: function() {
												//we don't need to do anything here 
											}
										}
									}
								});
							}
						},
						toTimesheet:{
							label: "Open Timesheet",
							className: "btn-primary",
							callback: function(){
								Timesheet.buttons.$weekViewRedirect.click();
							}
						}
					}
				});
			})

			this.$weekViewRedirect.on('click', function(e){
				waitingDialog.show(Timesheet.text.prepareRequest);
				Timesheet.activeAjaxRequest = $.ajax({
					url: '/tools/generateTimesheetURL',
					method: 'post',
					dataType: 'json'
				}).done((function(result){
					//console.log(result)
					if(result.status == true && result.url){
						var newWin = window.open(result.url, '_blank');
						waitingDialog.hide();
						if(!newWin || newWin.closed || typeof newWin.closed=='undefined'){ 
							bootbox.dialog({
								title: "New Window Alert",
								message: "We attempted to open the page you requested but your browser may have blocked the new window.  Please allow popups from this URL, and try again.",
								buttons: {
									default: {
										label: "OK, Thanks for the Update!",
										className: "btn-primary",
										callback: function() {}
									}
								}
							});
						}
					}
					Timesheet.activeAjaxRequest = null;
				}).bind(this));
			});

			$(document).on('change', '.project-status', function(e){
				var projectId = $(this).data('projectId');
				var status = $(this).val();
				var $otherObj = $('.project-status[data-project-id="'+projectId+'"][value="'+status+'"]').not(this);
				
				// this is the logic to toggle the other group of buttons.
				// this is currently not working.  we need to fix this!
				console.log(this);
				console.log($otherObj);
				$otherObj.prop('checked', true);
				$otherObj.parents('.btn-group').find('.btn').removeClass('active');
				$otherObj.parent('.btn').addClass('active');
				

				Timesheet.activeAjaxRequest = $.ajax({
					url: '/tools/markProject',
					method: 'post',
					dataType: 'json',
					data:{
						'projectid': projectId,
						'status': status
					}
				}).done((function(result){
					//console.log(result)
					
					Timesheet.activeAjaxRequest = null;
				}).bind(this));
			});

		}
	},
	$importTarget:null,
	$form: null,
	$importedDataForm:null,
	$table: null,
	$container:null,
	$dayProgressContainer: null,
	init:function(strDate, intTimesheetID){
		this.date = strDate;
		this.timesheetID = intTimesheetID;
		this.$datePicker = $('.date')
		this.$container = $('.container.interfaceUI');
		this.$form = $('#timegrid');
		this.timeInputs = 'input[name*="existingRecords"], input[name*="newRecords"]';
		this.buttons.$importFromToggl = $('#importFromTogglBtn');
		this.buttons.$submitToOa = $('.submitToOa');
		this.buttons.$submitTimesheet = $('.submitTimesheet');
		this.buttons.$addRow = $('.addRow');
		this.buttons.$clearCache = $('.clearCache');
		this.buttons.$weekViewRedirect = $('.weekViewRedirect');
		this.$table = $('table', this.$form);
		this.$importTarget = $('.importTarget');
		this.$dayProgressContainer = $('.dayProgress');
		this.views.init();
		this.buttons.init();
		var that = this;

		$(document).on('change', '#timegrid select, #timegrid input, #timegrid textarea', function(){
			that.buttons.$submitToOa.removeProp('disabled');
			$('body').addClass('unsaved-changes');
		})

		this.updateTimesheetTimeTotal();
		
		this.$container.on('click', '#acceptImportedTime', (function(e){
			waitingDialog.show(Timesheet.text.defaultSubmit);
			this.$importedDataForm.submit();
		}).bind(this));

		$(document).on('FormReady', (function(e){
			/*this.$importedDataForm.validate({
				debug: true,
				rules: {
					"select":{
						min: 1
					}
				}
			});*/
		}).bind(this));

		$('.createTimesheet').on('click', function(){
			waitingDialog.show(Timesheet.text.createTimesheet);
		})

		$('.jumpToDay').on('click', function(e){
			if($('body').is('.unsaved-changes')){
				e.preventDefault();
				bootbox.dialog({
				message: "You have unsaved changes down there, don'cha know?",
				title: "Hey Woah There!",
				buttons: {
					confirm: {
						label: "Save Changes",
						className: "btn-warning",
						callback: function() {
							$('#timegrid').submit();
							$('body').removeClass('unsaved-changes');
						}
					},
					default: {
						label: "Cancel",
						className: "btn-default",
						callback: function() {}
					}
				}
			});
			} else {
				waitingDialog.show(Timesheet.text.loadingFromAPI);
			}
			
		})
		
		$(document).on('blur', this.timeInputs, (function(e){
			this.updateTimesheetTimeTotal();
		}).bind(this));

		/*$('html').on('keydown', function(e){
			//console.log(e.keyCode);
			var functionKeys = [112,113,114,115,116,117,118,119,120,121,122,123];
			if(functionKeys.indexOf(e.keyCode) >= 0){
				e.preventDefault();
			}
		})*/

		this.$datePicker.datepicker({
			orientation: 'bottom',
			todayBtn: true, 
			todayHighlight: true,
			autoclose: true
		}).on('changeDate', function(e){
			waitingDialog.show(Timesheet.text.loadingFromAPI);
			var dateObj = new Date(e.date);
			var month = dateObj.getUTCMonth() + 1; //months from 1-12
			if(month < 10)month = "0"+month;
			var day = dateObj.getUTCDate();
			if(day < 10)day = "0"+day;
			var year = dateObj.getUTCFullYear();
			newdate = year + "-" + month + "-" + day;
			location.href="/timeimport/toggl/"+newdate;
		});
		

		$.each(this.$form.find('tbody tr td:first-child'),function(index,row){
			var ID = $(row).data('id');
			$(row).parent('tr').attr('data-id', ID);
			$(row).removeAttr('data-id')
		});

		if(this.$table.find('tbody').length == 0){
			$('<tbody />').appendTo(this.$table);
		}

		

		if(this.$table.length == 0){
			$.each(this.buttons, function(key,$btn){
				if($btn && $btn.length > 0){
					$btn.prop('disabled', 'disabled');
				}
			});
		}

		this.$container.on('change', 'select.task', function(){
			var clientProjectId = $(this).parents('tr').find('select.clientid').val();
			var taskId = $(this).val();
			var aryClientTask = clientProjectId.split(':');

			that.activeAjaxRequest = $.ajax({
				url: '/tools/setFavoriteTask',
				method: 'post',
				data: {projectid:aryClientTask[1], taskid:taskId},
				dataType: 'json'
			}).done((function(data){
				that.activeAjaxRequest = null;
			}).bind(this));
		});

		this.$container.on('change', '.clientid', function(){
			// console.log('clientid changed');

			var $tableRow = $(this).parents('tr');
			var $origionalDropdown = $('.task', $tableRow);
			var selectName = $origionalDropdown.find('select').attr('name');
			var currentVal = $(this).val();

			if(currentVal == 0 || currentVal == '' || currentVal == null || currentVal == 'Please Select') { 
				$origionalDropdown.empty();
				var $newDiv = $('<div />', {class:'form-group'}).appendTo($origionalDropdown);
				var $newSelect = $('<select />',{
					name: selectName,
					class: 'form-control task selectpicker',
					disabled: 'true'
				}).appendTo($newDiv);
				$newSelect.append($("<option />", {
					value: '',
					text: 'Select Client'
				}));
				Timesheet.initTypeAhead();
				return; 
			}

			if(typeof selectName === 'undefined'){
				var togglID = $tableRow.find('td.task').data('togglId');
				if(togglID){
					selectName = 'newImportRecord['+togglID+'][projecttask]';
				}
			}
			$origionalDropdown.html('<img src="/application/assets/img/loading.gif">');

			var IDs = this.value.split(':');
			if(that.activeAjaxRequest){
				that.activeAjaxRequest.abort();
			}
			that.activeAjaxRequest = $.ajax({
				url: '/tools/loadTaskcodes',
				method: 'post',
				data: {customerid:IDs[0], projectid:IDs[1]},
				dataType: 'json'
			}).done((function(data){
				if(data.status == true){
					$origionalDropdown.empty();
					var $newDiv = $('<div />', {class:'form-group'}).appendTo($origionalDropdown);
					var $newSelect = $('<select />',{
						name: selectName,
						class: 'form-control task selectpicker'
					}).appendTo($newDiv);

					$.each(data.result, function(value,aryData) {
						var objSelectData = {
							value: value,
							text: aryData.name
						};
						if(aryData.prefered){
							objSelectData['selected'] = 'selected';
						}
						$newSelect.append($("<option />", objSelectData));
					});
					$('.task', $tableRow).html($origionalDropdown.html());
					Timesheet.initTypeAhead();
				} else if(currentVal.length > 0){
					$(this).val('');
				} else {
					bootbox.alert("Unfortunately an error has occurred.  Please either change the dropdown, and change it back your selected value, or refresh the page.", function(){});
					//alert('An error occurred:', data.message, 'Pleae try again.');
					//location.href="/logout"
				}
				that.activeAjaxRequest = null;
			}).bind(this));
		});

		
		this.$form.on('click', '.delete-time-entry', function(e){
			var $tableRow = $(this).parents('tr');
			bootbox.dialog({
				message: "You said you wanted to delete an entire row. Are you sure?",
				title: "You really hate that row, don'cha?",
				buttons: {
					confirm: {
						label: "Yes, please",
						className: "btn-primary",
						callback: function() {
							if($tableRow.data('id')){
								//this was a time record that was brought over by OA.  We need to flag it for deletion
								that.$table.after($('<input />', {
									type: 'hidden',
									name: 'delete[]',
									value: $tableRow.data('id')
								}));
							}
							$tableRow.remove();
							that.buttons.$submitToOa.removeProp('disabled');
							Timesheet.updateTimesheetTimeTotal();
							$('body').removeClass('unsaved-changes');
						}
					},
					default: {
						label: "Golly No!",
						className: "btn-default",
						callback: function() {}
					}
				}
			});
		})
		this.initTypeAhead();

	}, 
	initTypeAhead: function(){
		$('.selectpicker:not(.typeAhead)').selectpicker({
			liveSearch : true,
			selectOnTab : true
		}).on('show.bs.select',function(e){
			$('.bs-searchbox:visible input').focus();

		}).addClass('typeAhead');
		// $('.clientid:not(.tab-control)').on('keydown', '.bs-searchbox input', function(e) { 
		// 	console.log('key pressed');
		// 	var keyCode = e.keyCode || e.which; 
		// 	if (keyCode == 9) { 
		// 		console.log('it was tab');
		//     	e.preventDefault(); 
		//     	var enter = jQuery.Event("keypress");
		// 		enter.which = 13; 
		// 		enter.keyCode = 13;
		// 		console.log('this is',this);
		//     	$(this).trigger(enter);
		// 	} 
		// }).addClass('tab-control');
		/* This was an attempt to fix the tab behavior not selecting the current value */
		
	},
	views:{
		templates: {
			timesheetRow: null,
			importTable: null
		},
		init:function(){
			this.templates.timesheetRow = Handlebars.compile($("#table-row").html());
			this.templates.importTable = Handlebars.compile($("#import-table").html());
		}
	}
	
}