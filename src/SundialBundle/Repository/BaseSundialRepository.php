<?php

namespace SundialBundle\Repository;


use Doctrine\ORM\EntityRepository;

abstract class BaseSundialRepository extends EntityRepository
{

    public function findOrfill($type){

    }

    /**
     * This function is designed to be used with findOrfill.
     * if the sundial entity needs to be filled from OA,
     * this function will be called and return a populated, saved Sundial entity
     * @param $oaEntity
     * @return mixed
     */
    abstract protected function fillSundialEntity($oaEntity);

}