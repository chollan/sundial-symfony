<?php

namespace SundialBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * TimeTypeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TimeTypeRepository extends EntityRepository
{
}
