<?php

namespace SundialBundle\Repository;


class CustomerRepository extends BaseSundialRepository
{

    /**
     * This function is designed to be used with findOrfill.
     * if the sundial entity needs to be filled from OA,
     * this function will be called and return a populated, saved OA entity
     * @param $oaEntity
     * @return mixed
     */
    function fillSundialEntity($oaEntity)
    {
        // TODO: Implement fillSundialEntity() method.
    }
}