<?php

namespace SundialBundle\Exception;

use OpenAir\DataTypes\Timesheet;

class MultipleTimesheetException extends \Exception
{
    private $aryTimesheets = [];

    // Redefine the exception so message isn't optional
    public function __construct($message, \Exception $previous = null) {
        // some code

        // make sure everything is assigned properly
        parent::__construct($message, 0, $previous);
    }

    public function __toString() {
        return "{$this->message}  (".implode(', '.$this->aryTimesheetIDs).")";
    }

    public function addTimesheet(Timesheet $timesheet){
        $this->aryTimesheets[] = $timesheet;
    }

    public function setTimesheets(array $aryTimesheets){
        foreach($aryTimesheets as $objTimesheet){
            if($objTimesheet instanceof Timesheet){
                $this->aryTimesheets[] = $objTimesheet;
            }
        }
    }

    public function getTimesheets(){
        return $this->aryTimesheets;
    }

}