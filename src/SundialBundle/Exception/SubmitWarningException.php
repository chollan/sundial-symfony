<?php

namespace SundialBundle\Exception;

class SubmitWarningException extends \Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message, Exception $previous = null) {
        // some code

        // make sure everything is assigned properly
        parent::__construct($message, 1003, $previous);
    }

    public function __toString() {
        return "{$this->message}  ({$this->code})";
    }

}