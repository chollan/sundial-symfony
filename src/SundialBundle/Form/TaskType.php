<?php

namespace SundialBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{
    private $em;
    private $container;
    private $token;
    private $timesheetService;

    function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->token = $this->container->get('security.token_storage')->getToken();
        $this->timesheetService = $this->container->get('app.timesheet_service');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $timetypes = $this->em->getRepository('SundialBundle:TimeType')->findAll();
        $defaultTimetype = null;
        $entityUtility = $this->container->get('app.entity_utility');

        $aryTimeTypes = [];
        foreach($timetypes as $objTimetype){
            $aryTimeTypes[$objTimetype->getId()] = $objTimetype->getType();
        }

        // let's get the currently staffed projects
        $day = $this->timesheetService->getDay();
        $aryAssignedProjects = [];
        if(!is_null($day)){
            $aryTimesheetData = $this->timesheetService->getTimesheetDayView($day, false, false);
            $defaultTimetype = array_filter($timetypes, function ($objTimeType) use ($aryTimesheetData) {
                return ($objTimeType->getId() == $aryTimesheetData['Timesheet']['default_timetypeid']);
            });
            $defaultTimetype = reset($defaultTimetype);
            $aryAssignedProjects = $aryTimesheetData['BookedHours']['bookedprojects'];
        }

        if(count($aryAssignedProjects) > 0){
            //$aryProjects = ['Staffed' => [], 'Available' => []];
            $aryProjectObjects = ['Staffed' => [], 'Available' => []];
        }else{
            //$aryProjects = [];
            $aryProjectObjects = [];
        }

        $aryProkectAssignments = $this->em->getRepository('SundialBundle:ProjectAssign')->getAssignedProjects($this->token->getUser()->getId());
        foreach($aryProkectAssignments as $objAssignment){
            $objProject = $objAssignment->getProject();
            if(count($aryAssignedProjects) > 0){
                if(in_array($objProject->getId(), $aryAssignedProjects)){
                    $aryProjectObjects['Staffed'][] = $objProject;
                }
                $aryProjectObjects['Available'][] = $objProject;
            }else{
                $aryProjectObjects[] = $objProject;
            }
        }

        $fcnSort = function($a,$b){
            return strcmp($a->getCustomer()->getName(), $b->getCustomer()->getName());
        };

        if(count($aryAssignedProjects) > 0){
            usort($aryProjectObjects['Staffed'], $fcnSort);
            usort($aryProjectObjects['Available'], $fcnSort);
        }else{
            usort($aryProjectObjects, $fcnSort);
        }

        $projectTaskRepo = $this->em->getRepository('SundialBundle:ProjectTask');

        $builder
            ->add('id', HiddenType::class)
            ->add('togglId', HiddenType::class)
            ->add('togglTime', HiddenType::class)
            ->add('togglProjectId', HiddenType::class)
            ->add('togglProjectName', HiddenType::class)
            ->add('timetype', EntityType::class, [
                'placeholder' => 'Select',
                'class' => 'SundialBundle:TimeType',
                'choices' => $timetypes,
                'data' => $defaultTimetype,
                'attr' => ['class' => 'timetype_select']
            ])
            ->add('project', EntityType::class, [
                'class' => 'SundialBundle:Project',
                'placeholder' => 'Select',
                'choices' => $aryProjectObjects,
                'attr' => ['class' => 'selectpicker'],
                'choice_label' => function ($proj) {
                    return $proj->getCustomer()->getName()." :: ".$proj->getName();
                }
            ])
            ->add('task', EntityType::class, [
                'class' => 'SundialBundle:ProjectTask',
                'placeholder' => 'Select Client First',
                'choices' => [],
                'attr' => ['disabled' => 'disabled']
            ])
            ->add('time', NumberType::class, [
                'precision' => 2,
                'attr' => ['step' => '.25', 'min' => 0, 'class' => 'time-entry']
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['cols' => 40, 'rows' => 1, 'class' => 'description-entry'],
                'required' => false
            ])
            ->add('mapped', CheckboxType::class, [
                'label' => false,
                'required' => false
            ])
        ;
        $container = $this->container;
        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event) use ($projectTaskRepo, $container, $entityUtility, $timetypes){
            $data = $event->getData();

            // remove the 'data' attribute on 'timetype' if there's form data.
            if($data && array_key_exists('timetype', $data) && !is_null($data['timetype'])){
                $form = $event->getForm();
                $form->remove('timetype');
                $form->add('timetype', EntityType::class, [
                    'placeholder' => 'Select',
                    'class' => 'SundialBundle:TimeType',
                    'choices' => $timetypes,
                    'attr' => ['class' => 'timetype_select']
                ]);
            }

            // override the projecttask if there's data
            if($data && array_key_exists('project', $data) && !is_null($data['project'])){
                $form = $event->getForm();
                $form->remove('task');
                $choices = $projectTaskRepo->getTasksForProject($data['project']);

                if(!$choices){
                    $choices = $entityUtility->fillTasksForProject($data['project']);
                }
                //dump($choices);exit;
                $form->add('task', EntityType::class, [
                    'class' => 'SundialBundle:ProjectTask',
                    'choices' => $choices,
                    'choice_label' => function ($task){
                        return $task->getIdNumber().'. '.$task->getName();
                    }
                ]);
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) use ($projectTaskRepo, $container, $entityUtility){
            $data = $event->getData();
            if(!is_null($data)){
                $choices = $projectTaskRepo->getTasksForProject($data['project']);
                if(!$choices){
                    $choices = $entityUtility->fillTasksForProject($data['project']);
                }
                $aryTasks = array_filter($choices, function($obj) use ($data){
                    return $obj->getId() == $data['task'];
                });
                $objTask = reset($aryTasks); // get the first and only object
                $event->getForm()->add('task', EntityType::class, [
                    'class' => 'SundialBundle:ProjectTask',
                    'choices' => $choices,
                    'data' => $objTask
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver){}

    public function getName()
    {
        return 'task_type';
    }

}