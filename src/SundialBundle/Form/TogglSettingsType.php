<?php

namespace SundialBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TogglSettingsType extends AbstractType
{
    private $em;
    private $container;
    private $token;

    function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->token = $this->container->get('security.token_storage')->getToken();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $timetypes = $this->em->getRepository('SundialBundle:TimeType')->findAll();
        $entityUtility = $this->container->get('app.entity_utility');

        $aryProkectAssignments = $this->em->getRepository('SundialBundle:ProjectAssign')->getAssignedProjects($this->token->getUser()->getId());
        $aryProjects = [];
        foreach($aryProkectAssignments as $objAssignment){
            $aryProjects[] = $objAssignment->getProject();
        }
        usort($aryProjects, function($a, $b){
            return strcmp($a->getCustomer()->getName(), $b->getCustomer()->getName());
        });

        $projectTaskRepo = $this->em->getRepository('SundialBundle:ProjectTask');

        $builder
            ->add('id', HiddenType::class)
            ->add('togglClient', HiddenType::class)
            ->add('togglProject', HiddenType::class)
            ->add('togglProjectId', HiddenType::class)
            ->add('timetype', EntityType::class, [
                'class' => 'SundialBundle:TimeType',
                'placeholder' => 'Select',
                'choices' => $timetypes,
                'required' => false,
            ])
            ->add('project', EntityType::class, [
                'class' => 'SundialBundle:Project',
                'placeholder' => 'Select',
                'choices' => $aryProjects,
                'required' => false,
                'choice_label' => function ($proj) {
                    return $proj->getCustomer()->getName()." :: ".$proj->getName();
                }
            ])
            ->add('task', EntityType::class, [
                'class' => 'SundialBundle:ProjectTask',
                'placeholder' => 'Select Client',
                'choices' => [],
                'required' => false,
                //'attr' => ['class' => 'selectpicker']
            ])
        ;
        $container = $this->container;
        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event) use ($projectTaskRepo, $container, $entityUtility){
            $data = $event->getData();
            if($data && array_key_exists('project', $data) && !is_null($data['project'])){
                $form = $event->getForm();
                $form->remove('task');
                $choices = $projectTaskRepo->getTasksForProject($data['project']);
                if(!$choices){
                    $choices = $entityUtility->fillTasksForProject($data['project']);
                }
                $form->add('task', EntityType::class, [
                    'class' => 'SundialBundle:ProjectTask',
                    'choices' => ($choices?$choices:[]),
                    'choice_label' => function ($task){
                        return $task->getIdNumber().'. '.$task->getName();
                    }
                ]);
            }else{

            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) use ($projectTaskRepo, $container, $entityUtility){
            $data = $event->getData();
            if(!is_null($data) && $data['project'] != ''){
                $choices = $projectTaskRepo->getTasksForProject($data['project']);
                if(!$choices){
                    $choices = $entityUtility->fillTasksForProject($data['project']);
                }
                $aryTasks = array_filter($choices, function($obj) use ($data){
                    return $obj->getId() == $data['task'];
                });
                $objTask = reset($aryTasks); // get the first and only object
                $event->getForm()->add('task', EntityType::class, [
                    'class' => 'SundialBundle:ProjectTask',
                    'choices' => $choices,
                    'data' => $objTask
                ]);
                /*if(empty($data['id']) && array_key_exists('task', $data) && $data['task'] != ''){
                    list($client, $project) = explode(':', $data['client']);
                    $choices = $projectTaskRepo->getTasksForProject($project);
                    if(!$choices){
                        $choices = $entityUtility->fillTasksForProject($project);
                    }
                    $event->getForm()->add('task', ChoiceType::class, [
                        'choices' => $choices,
                        'data' => $data['task']
                    ]);
                }*/
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver){}

    public function getName()
    {
        return 'toggl_settings_type';
    }

}