<?php

namespace SundialBundle\Form;


use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportType extends AbstractType
{

    private $em;
    private $container;
    private $token;
    private $timesheetService;
    private $user;

    function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->token = $this->container->get('security.token_storage')->getToken();
        $this->timesheetService = $this->container->get('app.timesheet_service');
        $this->user = $this->token->getUser();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $timetypes = $this->em->getRepository('SundialBundle:TimeType')->findAll();

        $aryTimeTypes = [];
        foreach($timetypes as $objTimetype){
            $aryTimeTypes[$objTimetype->getId()] = $objTimetype->getType();
        }

        $customerRepo = $this->em->getRepository('SundialBundle:Customer');
        $aryCustomers = $customerRepo->findByActive(true);
        $aryProjectsPicklist = [];
        foreach($aryCustomers as $objCustomer){
            $aryProjectsPicklist[$objCustomer->getId()] = $objCustomer->getName();
        }

        asort($aryProjectsPicklist);

        $builder
            ->add('name')
            ->add('customer', ChoiceType::class, [
                'placeholder' => 'Select',
                'choices' => $aryProjectsPicklist,
                'mapped' => false
            ])
            ->add('project', ChoiceType::class, [
                'placeholder' => 'Select Client',
                'choices' => [],
                'choices_as_values' => true
            ])
            ->add('budgetHigh', MoneyType::class, [
                'currency' => 'USD'
            ])
            ->add('budgetLow', MoneyType::class, [
                'currency' => 'USD'
            ])
            ->add('averageRate', MoneyType::class, [
                'currency' => 'USD'
            ])
            ->add('hoursToFinish', NumberType::class, [
                'attr' => ['min' => '0', 'step' => '.25'],
                'label' => 'Hours to Finish'
            ])
            ->add('create', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary btn-block',
                    'style' => 'margin-top:15px;'
                ]
            ])
        ;

        $projectRepo = $this->em->getRepository('SundialBundle:Project');

        $funcGetProjects = function($customerId) use ($projectRepo, $customerRepo){

            $projects = $projectRepo->findByCustomer(
                $customerRepo->find($customerId)
            );

            $arySortedProjects = [];
            foreach($projects as $objProj){
                $arySortedProjects[$objProj->getId()] = $objProj->getName();
            }
            asort($arySortedProjects);
            return $arySortedProjects;
        };

        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event) use ($funcGetProjects){
            $data = $event->getData();
            if($data && array_key_exists('customer', $data) && !is_null($data['customer'])){

                $form = $event->getForm();
                $form->remove('project');;
                $form->add('project', ChoiceType::class, [
                    'choices' => $funcGetProjects($data['customer']),
                ]);
            }
        });

        $user = $this->user;
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) use ($funcGetProjects, $user, $projectRepo){
            $data = $event->getData();
            if(!is_null($data)){
                if(empty($data['id']) && array_key_exists('project', $data) && $data['project'] != ''){
                    $form = $event->getForm();
                    $form->remove('project');
                    /*dump($projectRepo->find($data['project']));
                    dump($data);exit;*/
                    $form->add('project', ChoiceType::class, [
                        'choices' => $funcGetProjects($data['customer']),
                        'data' => $data['project']
                        //'data' => $projectRepo->find($data['project'])
                    ]);
                }
            }
        });

    }

    public function getName(){
        return 'financial_report_type';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'SundialBundle\Entity\FinancialReport',
        ]);
    }

}