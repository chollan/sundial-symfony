<?php

namespace SundialBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SundialBundle\Entity\TogglProjectSetting;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

class LoatTogglSettings extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityUtility = $this->container->get('app.entity_utility');
        $em = $this->container->get('doctrine')->getManager();
        /*
         * array:7 [
              0 => "id"
              1 => "togglprojectid"
              2 => "oauserid"
              3 => "oatimetypeid"
              4 => "oacustomerid"
              5 => "oaprjectid"
              6 => "oaprojecttaskid"
            ]*/

        $finder = new Finder();
        $finder->files()
            ->in('src/SundialBundle/DataFixtures/ORM/')
            ->name('TogglSettings.csv')
        ;
        foreach ($finder as $file) { $csv = $file; }

        $handle = $handle = fopen($csv->getRealPath(), "r");
        $header = fgetcsv($handle);
        $data = fgetcsv($handle);
        do{
            $togglSetting = new TogglProjectSetting();
            $togglSetting->setCustomer($entityUtility->fetchOrFill('Customer', $data[4]));
            $togglSetting->setProject($entityUtility->fetchOrFill('Project', $data[5]));
            $togglSetting->setTask($entityUtility->fetchOrFill('ProjectTask', $data[6]));
            $objTimeType = $entityUtility->fetchOrFill('TimeType', $data[3]);
            if(is_null($objTimeType)){
                dump($data);
                $data = fgetcsv($handle);
                continue;
            }
            $togglSetting->setTimetype($objTimeType);
            $togglSetting->setTogglProjectId($data[1]);
            $togglSetting->setUser($entityUtility->fetchOrFill('User', $data[2]));
            $em->persist($togglSetting);
            $data = fgetcsv($handle);
        }while($data !== false);

        $em->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 7;
    }
}