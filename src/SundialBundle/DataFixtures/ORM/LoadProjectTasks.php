<?php

namespace SundialBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OpenAir\Base\Command;
use OpenAir\Commands\Auth;
use OpenAir\Commands\Read;
use OpenAir\DataTypes\Login;
use SundialBundle\Entity\ProjectTask;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadProjectTasks extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $oldMemoryLimit  = ini_get("memory_limit");
        ini_set("memory_limit", "-1");
        $namespace = $this->container->getParameter('sundial.oa_namespace');
        $key = $this->container->getParameter('sundial.oa_key');
        $url = $this->container->getParameter('sundial.oa_server');
        $company = $this->container->getParameter('sundial.oa_company');
        $default_user = $this->container->getParameter('sundial.default_user');
        $default_pass = $this->container->getParameter('sundial.default_pass');
        $customerRepo = $this->container->get('doctrine')->getRepository('SundialBundle:Customer');
        $projectRepo = $this->container->get('doctrine')->getRepository('SundialBundle:Project');

        $objRequest = new \OpenAir\Request($namespace, $key, $url);
        //$objRequest->setDebug(true);

        // let's login
        $objRequest->addAuthCommand(new Auth(new Login($company, $default_user, $default_pass)));
        $limit = 0;

        //do {
            // read project tasks
            $objReadRequest = new Read([
                'type' => 'Projecttask',
                //'method' => Read::METHOD_EQUAL_TO,
                'method' => Read::METHOD_ALL,
                //'limit' => $limit.', 1000'
            ]);
            /*$objReadRequest->addDataType(new \OpenAir\DataTypes\Projecttask([
                'closed' => 0
            ]));*/
            $objReadRequest->setReturnValues([
                'id','updated','name','seq','parentid','id_number','projectid','customerid', 'is_a_phase', 'closed'
            ]);

            $objRequest->addCommand($objReadRequest);

            $objResponse = $objRequest->execute();

            $aryReadResponses = $objResponse->getCommandResponse('Read');
            $objReadResponse = $aryReadResponses[0]; // there's only 1
            if($objReadResponse->getResponseStatus() == Command::STATUS_SUCCESS){
                $aryReadResponseData = $objReadResponse->getResponseData();
                $aryParentIDs = [];
                $count = 0;
                foreach ($aryReadResponseData as $task) {
                    $objProjectTask = new ProjectTask();
                    $objProjectTask->setId($task->id);
                    $objCustomer = $customerRepo->find($task->customerid);
                    if(is_null($objCustomer)){
                        // the customer being null means
                        // that the customer is not active
                        continue;
                    }
                    $objProjectTask->setCustomer($objCustomer);
                    $objProjectTask->setIdNumber($task->id_number);
                    $objProjectTask->setIsPhase($task->is_a_phase);
                    $objProjectTask->setName($task->name);
                    $objProjectTask->setClosed($task->closed);
                    //$objProjectTask->setParent();
                    $objProject = $projectRepo->find($task->projectid);
                    if(is_null($objProject)){
                        // the project being null means
                        // the project is not active
                        continue;
                    }
                    $objProjectTask->setProject($objProject);
                    $objProjectTask->setUpdated(new \DateTime(date('Y-m-d H:i:s', $task->updated)));
                    $objProjectTask->setSequence($task->seq);

                    // let's set the parent ids at the end
                    // incase we come across a missing customer or project
                    // due to them being closed
                    if(!is_null($task->parentid)){
                        $aryParentIDs[$task->id] = $task->parentid;
                    }
                    $count++;
                    echo 'saving projecttask '.$task->name.' ('.$task->id.')'.PHP_EOL;
                    $manager->persist($objProjectTask);
                    if($count%50 === 0){
                        echo 'FLUSHING ...';
                        $manager->flush();
                        $manager->clear();
                        echo 'Done.'.PHP_EOL;
                    }

                }

                // let's populate parent ids now
                if(count($aryParentIDs)>0){
                    $manager->flush();
                    $projectTaskRepo = $this->container->get('doctrine')->getRepository('SundialBundle:ProjectTask');
                    foreach($aryParentIDs as $taskID => $parentID){
                        $parent = $projectTaskRepo->find($parentID);
                        $task = $projectTaskRepo->find($taskID);
                        if(($parent instanceof ProjectTask) && ($task instanceof ProjectTask)){
                            $task->setParent($parent);
                            $manager->persist($task);
                        }
                    }
                }
                $manager->flush();
            }
            $limit += 1000;
        //}while(count($aryReadResponseData) == 1000);
        ini_set("memory_limit", $oldMemoryLimit);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}
