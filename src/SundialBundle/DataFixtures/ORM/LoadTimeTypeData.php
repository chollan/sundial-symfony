<?php

namespace SundialBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OpenAir\Base\Command;
use OpenAir\Commands\Auth;
use OpenAir\Commands\Read;
use OpenAir\DataTypes\Login;
use SundialBundle\Entity\TimeType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTimeTypeData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function load(ObjectManager $manager){

        $namespace = $this->container->getParameter('sundial.oa_namespace');
        $key = $this->container->getParameter('sundial.oa_key');
        $url = $this->container->getParameter('sundial.oa_server');
        $company = $this->container->getParameter('sundial.oa_company');

        $objRequest = new \OpenAir\Request($namespace, $key, $url);
        //$objRequest->setDebug(true);

        // let's login
        $objRequest->addCommand(new Auth(new Login($company, 'curtis.holland', 'Windows95')));

        //call whoami
        //$objRequest->addCommand(new Whoami());

        //let's build a read request
        $objReadCommand = new Read(['type' => "Timetype",'method' => Read::METHOD_ALL]);

        // let's set the return vales
        $objReadCommand->setReturnValues([
            'id',
            'name',
            'picklist_label',
            'active',
            'updated'
        ]);

        //add the read request to the OARequest
        $objRequest->addCommand($objReadCommand);
        $objResponse = $objRequest->execute();

        $aryReadResponses = $objResponse->getCommandResponse('Read');
        $objReadResponse = $aryReadResponses[0]; // there's only 1
        if($objReadResponse->getResponseStatus() == Command::STATUS_SUCCESS){
            foreach($objReadResponse->getResponseData() as $objTimetype){
                if($objTimetype->active == 1){
                    echo 'creating timetype '.$objTimetype->picklist_label.PHP_EOL;
                    $objTime = new TimeType();
                    $objTime->setId($objTimetype->id);
                    $objTime->setType($objTimetype->picklist_label);
                    $objTime->setActive($objTimetype->active);
                    $objTime->setUpdated($objTimetype->updated);
                    $manager->persist($objTime);
                }

            }
            $manager->flush();
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
