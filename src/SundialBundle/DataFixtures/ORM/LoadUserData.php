<?php

namespace SundialBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OpenAir\Commands\Auth;
use OpenAir\Commands\Whoami;
use OpenAir\Commands\Read;
use OpenAir\Base\Command;
use OpenAir\DataTypes\Login;
use OpenAir\DataTypes\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $namespace = $this->container->getParameter('sundial.oa_namespace');
        $key = $this->container->getParameter('sundial.oa_key');
        $url = $this->container->getParameter('sundial.oa_server');
        $company = $this->container->getParameter('sundial.oa_company');

        $objRequest = new \OpenAir\Request($namespace, $key, $url);
        //$objRequest->setDebug(true);

        // let's login
        $objRequest->addCommand(new Auth(new Login($company, 'curtis.holland', 'Windows95')));

        //call whoami
        //$objRequest->addCommand(new Whoami());

        //let's build a read request
        $objReadCommand = new Read(['type' => "User",'method' => Read::METHOD_EQUAL_TO]);

        // we only want users who are active
        $objReadCommand->addDataType(new User([
            'active' => 1
        ]));

        // let's set the return vales
        $objReadCommand->setReturnValues([
            'id',
            'name',
            'nickname',
            'picklist_label',
            'job_codeid',
            'active'
        ]);

        //add the read request to the OARequest
        $objRequest->addCommand($objReadCommand);
        $objResponse = $objRequest->execute();

        $aryReadResponses = $objResponse->getCommandResponse('Read');
        $objReadResponse = $aryReadResponses[0]; // there's only 1
        if($objReadResponse->getResponseStatus() == Command::STATUS_SUCCESS){
            foreach($objReadResponse->getResponseData() as $objOAUser){
                echo 'creating user '.$objOAUser->nickname.' ('.$objOAUser->id.')'.PHP_EOL;
                $localUser = new \SundialBundle\Entity\User();
                $localUser->setNickname($objOAUser->nickname);
                $localUser->setPicklistLabel($objOAUser->picklist_label);
                $localUser->setJobcode($objOAUser->job_codeid);
                $localUser->setId($objOAUser->id);
                $manager->persist($localUser);
            }
            $manager->flush();
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
