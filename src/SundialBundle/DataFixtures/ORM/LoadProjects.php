<?php
/**
 * Created by PhpStorm.
 * User: curtis.holland
 * Date: 12/7/2016
 * Time: 10:51 AM
 */

namespace SundialBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OpenAir\Base\Command;
use OpenAir\Commands\Auth;
use OpenAir\Commands\Read;
use OpenAir\DataTypes\Login;
use SundialBundle\Entity\Project;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadProjects extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $namespace = $this->container->getParameter('sundial.oa_namespace');
        $key = $this->container->getParameter('sundial.oa_key');
        $url = $this->container->getParameter('sundial.oa_server');
        $company = $this->container->getParameter('sundial.oa_company');
        $default_user = $this->container->getParameter('sundial.default_user');
        $default_pass = $this->container->getParameter('sundial.default_pass');
        $customerRepo = $this->container->get('doctrine')->getRepository('SundialBundle:Customer');

        $objRequest = new \OpenAir\Request($namespace, $key, $url);
        //$objRequest->setDebug(true);

        // let's login
        $objRequest->addAuthCommand(new Auth(new Login($company, $default_user, $default_pass)));

        $objReadCommand = new Read(['type' => 'project']);
        $objReadCommand->setReturnValues([
            'id',
            'name',
            'active',
            'updated',
            'customerid'
        ]);

        $objRequest->addCommand($objReadCommand);
        $objResponse = $objRequest->execute();

        $goodCount = 0;
        $badCount = 0;

        $aryReadResponses = $objResponse->getCommandResponse('Read');
        $objReadResponse = $aryReadResponses[0]; // there's only 1
        if($objReadResponse->getResponseStatus() == Command::STATUS_SUCCESS){
            $aryProjectData = $objReadResponse->getResponseData();
            foreach($aryProjectData as $project){
                echo "saving project ".$project->name." (".$project->id.")".PHP_EOL;
                $objProject = new Project();
                $objProject->setId($project->id);
                $objProject->setActive($project->active);
                $objCustomer = $customerRepo->find($project->customerid);
                if(is_null($objCustomer)){
                    $badCount++;
                    continue;
                }
                $objProject->setCustomer($objCustomer);
                $objProject->setName($project->name);
                $objProject->setUpdated(new \DateTime(date('Y-m-d H:i:s', $project->updated)));
                $manager->persist($objProject);
                $goodCount++;
            }
            $manager->flush();
        }
        echo "complete: ".$goodCount." good, ".$badCount." missing".PHP_EOL;
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 4;
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
