<?php

namespace SundialBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OpenAir\Base\Command;
use OpenAir\Commands\Auth;
use OpenAir\Commands\Read;
use OpenAir\DataTypes\Date;
use OpenAir\DataTypes\Login;
use OpenAir\Response;
use SundialBundle\Entity\ProjectTaskAssign;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadProjectTaskAssignments extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        return;
        $oldMemoryLimit  = ini_get("memory_limit");
        ini_set("memory_limit", "-1");
        $namespace = $this->container->getParameter('sundial.oa_namespace');
        $key = $this->container->getParameter('sundial.oa_key');
        $url = $this->container->getParameter('sundial.oa_server');
        $company = $this->container->getParameter('sundial.oa_company');
        $default_user = $this->container->getParameter('sundial.default_user');
        $default_pass = $this->container->getParameter('sundial.default_pass');
        $projectTaskRepo = $this->container->get('doctrine')->getRepository('SundialBundle:ProjectTask');
        $userRepo = $this->container->get('doctrine')->getRepository('SundialBundle:User');

        $objRequest = new \OpenAir\Request($namespace, $key, $url);
        //$objRequest->setDebug(true);

        // let's login
        $objRequest->addAuthCommand(new Auth(new Login($company, $default_user, $default_pass)));

        // read project tasks
        $objReadRequest = new Read(['type' => 'Projecttaskassign','method' => Read::METHOD_ALL]);
        $objReadRequest->setReturnValues(['id','userid','projecttaskid', 'updated']);
        $objRequest->addCommand($objReadRequest);
        
        // execute the request
        $objResponse = $objRequest->execute();
        
        //process the response
        if($objResponse instanceof Response){
            $aryReadResponses = $objResponse->getCommandResponse('Read');
            $objReadResponse = $aryReadResponses[0]; // there's only 1
            if($objReadResponse->getResponseStatus() == Command::STATUS_SUCCESS){
                $aryReadResponseData = $objReadResponse->getResponseData();
                $count = 0;
                foreach ($aryReadResponseData as $taskAssign) {
                    echo 'adding project task assingment id '.$taskAssign->id.PHP_EOL;
                    $objTaskAssign = new ProjectTaskAssign();
                    $objTaskAssign->setId($taskAssign->id);
                    $objTaskAssign->setUpdated($taskAssign->updated);

                    // find the projecttask object
                    $data = $projectTaskRepo->find($taskAssign->projecttaskid);
                    if(is_null($data))
                        continue;
                    $objTaskAssign->setProjectTask($data);

                    //find the user
                    $data = $userRepo->find($taskAssign->user);
                    if(is_null($data))
                        continue;
                    $objTaskAssign->setUser($data);
                    $manager->persist($objTaskAssign);
                    $count++;
                    if($count%50 === 0){
                        echo 'FLUSHING ...';
                        $manager->flush();
                        $manager->clear();
                        echo 'Done.'.PHP_EOL;
                    }
                }
                $manager->flush();
            }
        }
        ini_set("memory_limit", $oldMemoryLimit);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 6;
    }
}