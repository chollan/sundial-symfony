<?php

namespace SundialBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OpenAir\Base\Command;
use OpenAir\Commands\Auth;
use OpenAir\Commands\Read;
use OpenAir\DataTypes\Login;
use SundialBundle\Entity\Customer;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCustomers extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $namespace = $this->container->getParameter('sundial.oa_namespace');
        $key = $this->container->getParameter('sundial.oa_key');
        $url = $this->container->getParameter('sundial.oa_server');
        $company = $this->container->getParameter('sundial.oa_company');

        $objRequest = new \OpenAir\Request($namespace, $key, $url);
        //$objRequest->setDebug(true);

        // let's login
        $objRequest->addAuthCommand(new Auth(new Login($company, 'curtis.holland', 'Windows95')));
        
        $objReadCommand = new Read(['type' => 'customer']);
        $objReadCommand->setReturnValues(['id','name','active','updated']);

        $objRequest->addCommand($objReadCommand);
        $objResponse = $objRequest->execute();

        $aryReadResponses = $objResponse->getCommandResponse('Read');
        $objReadResponse = $aryReadResponses[0]; // there's only 1
        if($objReadResponse->getResponseStatus() == Command::STATUS_SUCCESS){
            $aryCustomerData = $objReadResponse->getResponseData();
            foreach($aryCustomerData as $customer){
                echo "saving customer ".$customer->name." (".$customer->id.")".PHP_EOL;
                $objCustomer = new Customer();
                $objCustomer->setId($customer->id);
                $objCustomer->setActive($customer->active);
                $objCustomer->setName($customer->name);
                $objCustomer->setUpdated(new \DateTime(date('Y-m-d H:i:s', $customer->updated)));
                $manager->persist($objCustomer);
            }
            $manager->flush();
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
