<?php

namespace SundialBundle\Annotations;

/**
 * Class OpenAirField
 * @package SundialBundle\Annotations
 * @Annotation
 * @Target({"PROPERTY"})
 */
class OpenAirField
{
    public $value;
    public $target;
    public $type;

    function __construct($options)
    {
        $this->value = $options['value'];
        if(array_key_exists('target', $options)){
            $this->target = $options['target'];
        }
        if(array_key_exists('type', $options)){
            $this->type = $options['type'];
        }
    }
}