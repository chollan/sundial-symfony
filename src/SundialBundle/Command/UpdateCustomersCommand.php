<?php

namespace SundialBundle\Command;


use SundialBundle\Command\AbstractUpdate\AbstractOAUpdateCommand;

class UpdateCustomersCommand extends AbstractOAUpdateCommand
{

    protected $TYPE = 'Customer';

    function configure()
    {
        $this->setName("sundial:update:customer")
            ->setDescription('import all projects tasks that have been recently updated');
    }
}