<?php

namespace SundialBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateAllCommand extends ContainerAwareCommand
{

    function configure()
    {
        $this->setName("sundial:update:all")
            ->setDescription('import all updated items from OA');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach(['customer', 'project', 'projecttask'] as $strCommand){
            $command = $this->getApplication()->find('sundial:update:'.$strCommand);
            $command->run($input, $output);
        }
    }

}