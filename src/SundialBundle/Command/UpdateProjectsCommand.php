<?php

namespace SundialBundle\Command;


use SundialBundle\Command\AbstractUpdate\AbstractOAUpdateCommand;
use SundialBundle\Entity\Project;

class UpdateProjectsCommand extends AbstractOAUpdateCommand
{

    protected $TYPE = 'Project';

    function configure()
    {
        $this->setName("sundial:update:project")
            ->setDescription('import all projects that have been recently updated');
    }
}