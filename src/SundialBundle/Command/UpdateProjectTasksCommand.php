<?php

namespace SundialBundle\Command;


use SundialBundle\Command\AbstractUpdate\AbstractOAUpdateCommand;

class UpdateProjectTasksCommand extends AbstractOAUpdateCommand
{

    protected $TYPE = 'ProjectTask';

    function configure()
    {
        $this->setName("sundial:update:projecttask")
            ->setDescription('import all projects tasks that have been recently updated');
    }
}