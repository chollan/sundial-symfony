<?php
/**
 * Created by PhpStorm.
 * User: cholland
 * Date: 12/26/16
 * Time: 11:40 AM
 */

namespace SundialBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RunReportCommand extends ContainerAwareCommand
{
    function configure()
    {
        $this->setName("sundial:report:finance:run")
            ->addArgument('id', InputArgument::REQUIRED, 'Identifier of the command to run')
            ->setDescription('Run a report');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repo = $em->getRepository('SundialBundle:FinancialReport');
        $report = $repo->find($id);

        if(is_null($report)){
            $output->writeln('<error>No report is found with the ID of '.$id.'</error>');
            exit;
        }

        $objReportService = $this->getContainer()->get('app.financial_report_service');
        $objReportService->runReport($report);
    }

}