<?php

namespace SundialBundle\Command\AbstractUpdate;


use Doctrine\Common\Annotations\AnnotationReader;
use OpenAir\Base\Command;
use OpenAir\Commands\Read;
use OpenAir\DataTypes\Date;
use OpenAir\DataTypes\Project;
use OpenAir\Response;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractOAUpdateCommand extends ContainerAwareCommand
{
    protected $container;
    protected $em;

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->container = $this->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
        $objRepo = $this->container->get('doctrine')->getRepository('SundialBundle:'.ucfirst($this->TYPE));
        $objEntityUtility = $this->container->get('app.entity_utility');

        $objEntity = $objRepo->findOneBy([], ['updated' => 'DESC']);
        $objDate = $objEntity->getUpdated();
        $objDate->add(new \DateInterval('PT15M')); // tasks are returning incorrectly
        //TODO: Contact OA About this isue

        $output->write('Fetching '.$this->TYPE.'s updated after '.$objDate->format('Y-m-d H:i:s').'... ');

        $objOpenAirService = $this->container->get('app.openair_service');
        $objRequest = $objOpenAirService->getRequest();

        $objReadRequest = new Read([
            'type' => ucfirst(strtolower($this->TYPE)),
            'method' => Read::METHOD_ALL,
            'filters' => [
                'filter' => 'newer-than',
                //'field' => 'updated'
            ]
        ]);

        $strClassName = 'SundialBundle\\Entity\\'.$this->TYPE;
        $aryFieldMaps = $this->__buildOAFields($strClassName);
        //dump($aryFieldMaps);
        $objReadRequest->setReturnValues(array_keys($aryFieldMaps));

        $objReadRequest->addDataType(new Date(
            $objDate->format('H'),
            $objDate->format('m'),
            $objDate->format('s'),
            $objDate->format('m'),
            $objDate->format('d'),
            $objDate->format('Y')
        ));

        //$objRequest->setDebug(true);
        $objRequest->addCommand($objReadRequest);
        $objResponse = $objRequest->execute();
        if($objResponse instanceof Response){
            $output->write('Done', true);
            $aryReadResponses = $objResponse->getCommandResponse('Read');
            $objReadResponse = $aryReadResponses[0]; // there's only 1
            if($objReadResponse->getResponseStatus() == Command::STATUS_SUCCESS){
                $aryUpdates = $objReadResponse->getResponseData();
                $output->writeln('Found <info>'.count($aryUpdates).'</info> '.$this->TYPE.'s to update');
                if(count($aryUpdates) > 0){
                    foreach($aryUpdates as $oaEntity){
                        $output->write('Working with '.$this->TYPE.' id <info>'.$oaEntity->id.'</info>... ');
                        $objSundialEntity = $objRepo->find($oaEntity->id);
                        $bContinue = true;
                        if($objSundialEntity){
                            $output->write('found '.$this->TYPE.', updating... ');
                        }else{
                            $output->write('found not found, creating... ');
                            $objSundialEntity = new $strClassName();
                            $bContinue = false;
                        }
                        foreach($aryFieldMaps as $strOaFieldName => $arySundialMapData){
                            if($bContinue && $arySundialMapData['sundialField'] == 'id')continue; // this won't ever change
                            $strFunction = 'set'.ucfirst($arySundialMapData['sundialField']);
                            if(is_null($arySundialMapData['target'])){
                                $objSundialEntity->$strFunction($oaEntity->$strOaFieldName);
                            }else{
                                $id = $oaEntity->{$strOaFieldName};
                                $objSundialEntity->$strFunction(
                                    $objEntityUtility->fetchOrFill($arySundialMapData['target'], $id)
                                );
                            }
                        }
                        $this->em->persist($objSundialEntity);
                        $output->writeLn('done');
                    }
                    $this->em->flush();
                }

            }
        }else{
            $output->writeLn('<error>Error connecting to API (HTTP code '.$objResponse.')</error>');
        }
    }

    private function __buildOAFields($entityClass){
        $reader = new AnnotationReader();
        $aryMetadata = $this->em->getClassMetadata($entityClass);
        $aryReturn = [];
        foreach($aryMetadata->getReflectionProperties() as $fieldName => $objReflectionClass){
            $oaField = $reader->getPropertyAnnotation($objReflectionClass, 'SundialBundle\Annotations\OpenAirField');

            if(!is_null($oaField)){
                $aryReturn[$oaField->value] = ['sundialField' => $fieldName, 'target' => $oaField->target, 'type' => $oaField->type];
            }
        }
        return $aryReturn;
    }
}
