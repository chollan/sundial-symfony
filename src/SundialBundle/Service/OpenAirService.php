<?php

namespace SundialBundle\Service;


use OpenAir\Commands\Auth;
use OpenAir\DataTypes\Login;
use OpenAir\Request;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;

class OpenAirService
{

    protected $container;
    protected $token;
    protected $namespace;
    protected $key;
    protected $server;
    protected $company;
    protected $user;
    protected $request;
    protected $em;
    protected $logger;

    function __construct(Container $container)
    {
        $this->container = $container;
        $this->token = $this->container->get('security.token_storage')->getToken();
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->logger = $this->container->get('monolog.logger.oa_xml');
        if(!($this->token instanceof AnonymousToken) && !is_null($this->token)){
            $this->user = $this->token->getUser();
            $this->username = $this->token->getUser()->getNickname();
            //$this->pass = $this->token->getAttribute('oapass');
            $this->pass = $this->token->getUser()->getPassword();
        }else{
            $this->username = $this->container->getParameter('sundial.default_user');
            $this->pass = $this->container->getParameter('sundial.default_pass');
        }


        $this->namespace = $this->container->getParameter('sundial.oa_namespace');
        $this->key = $this->container->getParameter('sundial.oa_key');
        $this->server = $this->container->getParameter('sundial.oa_server');
        $this->company = $this->container->getParameter('sundial.oa_company');

        //let's prepare the requests
        $this->request = new Request($this->namespace, $this->key, $this->server, '1.0');
        //$this->request->setDebug(true);

        $this->request->addAuthCommand(
            new Auth(
                new Login($this->company, $this->username, $this->pass)
            )
        );
    }

    function getRequest(){
        return $this->request;
    }

    protected function executeRequest(){
        $time = time();
        $this->logger->debug(
            //$response->getXMLRequest(),
            preg_replace('/(<password>)([^<]*)(<\/password>)/imsx','$1*****$3',$this->request->getXMLRequest()),
            [$time]
        );
        $response = $this->request->execute();
        $this->logger->debug(
            $response->getXMLResponse(),
            [$time]
        );
        return $response;
    }
}