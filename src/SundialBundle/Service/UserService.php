<?php

namespace SundialBundle\Service;


use OpenAir\Base\Command;
use OpenAir\Commands\Read;
use OpenAir\DataTypes\Date;
use OpenAir\Response;
use SundialBundle\Entity\ProjectAssign;
use SundialBundle\Entity\User;
use Symfony\Component\DependencyInjection\Container;

class UserService extends OpenAirService
{

    function __construct(Container $container)
    {
        parent::__construct($container);
    }

    public function fillProjectAssignments(User $user){
        $objProjectAssignRepo = $this->em->getRepository('SundialBundle:ProjectAssign');
        $objProjectTaskAssign = $objProjectAssignRepo->findOneBy(['user' => $user],['updated' => 'DESC']);
        if(is_null($objProjectTaskAssign)){
            // this user has never logged in,
            // we need to store their assignment data
            $objReadCommand = new Read([
                'type' => 'projectassign',
                'method' => Read::METHOD_EQUAL_TO
            ]);
        }else{
            // this user previously logged in
            // we need to update their assignments
            $objReadCommand = new Read([
                'type' => 'projectassign',
                'method' => Read::METHOD_EQUAL_TO,
                'filters' => [
                    'filter' => 'newer-than',
                    'field' => 'updated'
                ]
            ]);
            $objReadCommand->addDataType(new Date(
                $objProjectTaskAssign->getUpdated()->format('H'),
                $objProjectTaskAssign->getUpdated()->format('i'),
                $objProjectTaskAssign->getUpdated()->format('s'),
                $objProjectTaskAssign->getUpdated()->format('m'),
                $objProjectTaskAssign->getUpdated()->format('d'),
                $objProjectTaskAssign->getUpdated()->format('Y')
            ));
        }

        $objReadCommand->addDataType(new \OpenAir\DataTypes\Projectassign([
            'user_id' => $user->getId()
        ]));

        $objReadCommand->setReturnValues([
            'id',
            'project_id',
            'customer_id',
            'updated'
        ]);

        $this->request->addCommand($objReadCommand);
        //$this->request->setDebug(true);

        $objResult = $this->executeRequest();


        if($objResult instanceof Response){
            $aryReadResults = $objResult->getCommandResponse('Read');
            $objReadResult = $aryReadResults[0]; //there's only 1
            if($objReadResult->getResponseStatus() == Command::STATUS_SUCCESS){
                $aryReadResultData = $objReadResult->getResponseData();
                $objEntityUtility = $this->container->get('app.entity_utility');
                foreach($aryReadResultData as $assignment){
                    $objAssignment = $objProjectAssignRepo->find($assignment->id);
                    $bNew = false;
                    if(is_null($objAssignment)){
                        $objAssignment = new ProjectAssign();
                        $objAssignment->setUser($user);
                        $objAssignment->setId($assignment->id);
                        $bNew = true;
                    }
                    $objAssignment->setCustomer($objEntityUtility->fetchOrFill('Customer', $assignment->customer_id));
                    $objAssignment->setUpdated(new \DateTime(date('Y-m-d H:i:s', $assignment->updated)));
                    $objAssignment->setProject($objEntityUtility->fetchOrFill('Project', $assignment->project_id));
                    if($bNew)
                        $this->em->persist($objAssignment);
                    else
                        $this->em->merge($objAssignment);

                }
                $this->em->flush();
            }
        }
        // exit;
        return true;
    }

}
