<?php

namespace SundialBundle\Service;

use OpenAir\Base\Command;
use OpenAir\Commands\Add;
use OpenAir\Commands\Delete;
use OpenAir\Commands\MakeURL;
use OpenAir\Commands\Modify;
use OpenAir\Commands\Read;
use OpenAir\Commands\Submit;
use OpenAir\DataTypes\Booking;
use OpenAir\DataTypes\Date;
use OpenAir\DataTypes\Task;
use OpenAir\DataTypes\Timesheet;
use OpenAir\Response;
use SundialBundle\Entity\TimeType;
use SundialBundle\Entity\TogglImportedRecord;
use SundialBundle\Exception\MultipleTimesheetException;
use SundialBundle\Exception\SubmitErrorException;
use SundialBundle\Exception\SubmitWarningException;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilder;

class TimesheetService extends OpenAirService
{

    private $session;
    private $day;

    function __construct(Container $container){
        parent::__construct($container);
        $this->session = $this->container->get('session');
    }

    public function getDay(){
        return $this->day;
    }

    public function setDay(\DateTime $day){
        $this->day = $day;
    }

    public function getTimesheetDayView(\DateTime $day, $clearCache = false, $doFetch = true){
        $strSessionKey = 'timesheet.'.$this->getWeekStart($day->format('U'));
        $this->day = $day;

        if($this->session->has($strSessionKey) && $clearCache){
            $this->session->remove($strSessionKey);
        }

        if($this->session->has($strSessionKey) && !$clearCache){
            $arySessionData = $this->session->get($strSessionKey);
            $arySessionData['URL'] = $this->session->get('timesheet.url');
            $arySessionData['RejectedTimesheets'] = $this->session->get('timesheet.rejected');
            return $arySessionData;
        }elseif($this->session->has($strSessionKey) && $clearCache){
            // let's just update the tasks for the day
            // we already have timesheet data

            $this->_getTasksForWeekCommand($day);

            $result = $this->executeRequest();
            $objAuthResponse = $result->getCommandResponse('Auth');
            if($result instanceof Response){
                if($objAuthResponse->getResponseStatus() == Command::STATUS_SUCCESS){
                    $aryReadResponses = $result->getCommandResponse('Read');
                    $aryCachedTimesheet = $this->session->get($strSessionKey);
                    $aryCachedTimesheet['Tasks'] = $this->_getTasksForWeekCommand($aryReadResponses[0]);
                    $this->session->set($strSessionKey, $aryCachedTimesheet);
                    return $aryCachedTimesheet;
                }else{
                    // auth unsuccessful
                }
            }else{
                // request failed
                return 'Open Air request failed';
            }
        }else{
            if($doFetch){
                $this->_getTimesheetForWeekCommand($day); //0
                $this->_getOtherTimesheets($day); //1,2 <-- this will contain 2 requests
                $this->_getTasksForWeekCommand($day); //3
                $this->_getBookedHoursForWeekCommand($day); //4
                if(!$this->session->has('timesheet.rejected')) {
                    $this->_getRejectedTimeshetsCommand(); //5
                }
                if(!$this->session->has('timesheet.url')) {
                    $this->_getTimesheetURL(); //6
                }

                //$this->request->setDebug(true);
                $result = $this->executeRequest();
                //exit;

                $objAuthResponse = $result->getCommandResponse('Auth');
                if($result instanceof Response){
                    if($objAuthResponse->getResponseStatus() == Command::STATUS_SUCCESS){
                        // the authentication was successful
                        // let's get all the read responses
                        $aryReadResponses = $result->getCommandResponse('Read');

                        $aryReturn = [];
                        $aryReturn['Timesheet'] = $this->_getTimesheetForWeekCommand($aryReadResponses[0]);
                        $aryReturn['OtherTimesheets'] = $this->_getOtherTimesheets([$aryReadResponses[1], $aryReadResponses[2]]);
                        $aryReturn['Tasks'] = $this->_getTasksForWeekCommand($aryReadResponses[3], $aryReturn['OtherTimesheets'], $aryReturn['Timesheet']);
                        $aryReturn['BookedHours'] = $this->_getBookedHoursForWeekCommand($aryReadResponses[4], $aryReadResponses[3], $aryReturn['Timesheet']['total']);
                        if(!$this->session->has('timesheet.rejected')){
                            $this->session->set('timesheet.rejected', $this->_getRejectedTimeshetsCommand($aryReadResponses[5]));
                        }
                        if(!$this->session->has('timesheet.url')){
                            $this->session->set('timesheet.url', $this->_getTimesheetURL($result->getCommandResponse('MakeURL')));
                        }

                        $aryReturn['TogglIsLinked'] = $this->_isTogglLinked();
                        $this->session->set($strSessionKey, $aryReturn);

                        // we're not storing the URL or RejectedTimesheets
                        $aryReturn['URL'] = $this->session->get('timesheet.url');
                        $aryReturn['RejectedTimesheets'] = $this->session->get('timesheet.rejected');
                        return $aryReturn;
                    }else{
                        // auth unsuccessful
                    }
                }else{
                    // request failed
                    return 'Open Air request failed';
                }
            }else{
                return null;
            }

        }
    }

    public function getCreateTimesheetForm(FormBuilder $builder){
        $timetypes = $this->em->getRepository('SundialBundle:TimeType')->findAll();

        $aryTimeTypes = [];
        foreach($timetypes as $objTimetype){
            $aryTimeTypes[$objTimetype->getId()] = $objTimetype->getType();
        }

        $builder->add('timeType', ChoiceType::class, [
            'required' => false,
            'placeholder' => 'Type',
            'choices' => $aryTimeTypes,
            'label' => false
        ]);
        $builder->add('submit', SubmitType::class, [
            'label' => 'BAM!',
            'glyphicon' => 'glyphicon-time',
            'attr'=>['class'=>'btn btn-success']
        ]);

        return $builder->getForm();
    }

    public function createAndRefreshTimesheet(\DateTime $day, $intDefaultTimeType=null){
        $intStartDate = $this->getWeekStart($day->format('U'));
        $intEndDate = $intStartDate+604799;

        $timesheetAttribute = [
            'userid' => $this->user->getId(),
            'starts' => new Date(null, null, null, date('m', $intStartDate), date('d', $intStartDate), date('Y', $intStartDate)),
            'ends' => new Date(null, null, null, date('m', $intEndDate), date('d', $intEndDate), date('Y', $intEndDate)),
        ];
        if(!is_null($intDefaultTimeType)){
            $timesheetAttribute['default_timetypeid'] = $intDefaultTimeType;
        }

        $objTimesheet = new Timesheet($timesheetAttribute);
        $objAddCommand = new Add(['type' => 'Timesheet']);
        $objAddCommand->addDataType($objTimesheet);
        $this->request->addCommand($objAddCommand);

        $this->clearLocalCache();
        $this->container->get('app.event_recorder')->RecordEvent($this->user, UserEventRecorder::CREATE_TIMESHEET);
        return $this->getTimesheetDayView($day);
    }

    public function submitAndRefreshTimesheet(array $aryData, \DateTime $day){
        $aryTimesheetCache = $this->getTimesheetDayView($day);
        $bTogglImport = false;

        // let's detect if we're dealing with a toggl import
        // if we are, we need to merge the data first then process below
        if(count($aryData) >= 1 && is_null($aryData[0]['id']) && !is_null($aryData[0]['togglId'])){

            $objTogglService = $this->container->get('app.toggl_service');

            $bTogglImport = true;
            $aryMergedData = [];
            foreach($aryData as $aryDataToMerge){
                if(array_key_exists('mapped', $aryDataToMerge)){
                    if(!empty($aryDataToMerge['togglProjectId'])){
                        if($aryDataToMerge['mapped'] === true){
                            $objTogglService->updateCreateSetting(
                                $aryDataToMerge['timetype'],
                                $aryDataToMerge['project'],
                                $aryDataToMerge['task'],
                                $this->user,
                                $aryDataToMerge['togglProjectId']
                            );
                        }else{
                            $objTogglService->removeSetting(
                                $this->user,
                                $aryDataToMerge['togglProjectId']
                            );
                        }
                    }
                }
                $key = $aryDataToMerge['timetype']->getId().':'.$aryDataToMerge['project']->getCustomer()->getId().':'.$aryDataToMerge['task']->getId();
                if(!array_key_exists($key, $aryMergedData)){
                    $aryMergedData[$key] = $aryDataToMerge;
                }else{
                    $aryMergedData[$key]['togglTime'] += $aryDataToMerge['togglTime'];
                    $aryMergedData[$key]['time'] += $aryDataToMerge['time'];
                    $aryMergedData[$key]['description'] .= ' ; '.trim($aryDataToMerge['description']);
                }

                $obj = new TogglImportedRecord($aryDataToMerge['togglId'], $this->user);
                $this->em->persist($obj);
            }
            $this->em->flush();

            // now let's populate the correct time
            foreach($aryMergedData as $strUniqueKey => $taskData){
                $aryMergedData[$strUniqueKey]['time'] = $this->_convertSecondsToDecimal($aryMergedData[$strUniqueKey]['togglTime']);
                unset($aryMergedData[$strUniqueKey]['togglTime']);
            }
            $aryData = array_values($aryMergedData);
        }

        if($bTogglImport){
            if(count($aryTimesheetCache['Tasks']['daybreakdown']) > 0) {
                foreach ($aryTimesheetCache['Tasks']['daybreakdown'][$day->format('D')]['tasks'] as $objOATask) {
                    foreach($aryData as $key => $passableData){
                        if(
                            $passableData['timetype']->getId() == $objOATask->timetypeid  &&
                            $passableData['project']->getCustomer()->getId() == $objOATask->customerid  &&
                            $passableData['project']->getId() == $objOATask->projectid  &&
                            $passableData['task']->getId() == $objOATask->projecttaskid
                        ){
                            // if we're in here, then there's a record that should be merged and updated
                            $aryDataType = [
                                'id' => $objOATask->id,
                                'decimal_hours' => (floatval($objOATask->decimal_hours) + $passableData['time']),
                                'description' => $objOATask->description.' ; '.trim($passableData['description'])
                            ];
                            $command = new Modify(['type' => 'Task']);
                            $command->addDataType(new Task($aryDataType));
                            $this->request->addCommand($command);
                            unset($aryData[$key]);
                            continue;
                        }
                    }
                }
            }

            // for the remaining items, let's create new records
            foreach($aryData as $passableData){
                $command = new Add(['type' => 'Task']);
                $command->addDataType(new Task([
                    'userid' => $this->user->getId(),
                    'date' => new Date(null, null, null, $day->format('m'), $day->format('d'), $day->format('Y')),
                    'decimal_hours' => $passableData['time'],
                    'timetypeid' => $passableData['timetype']->getId(),
                    'projectid' => $passableData['project']->getId(),
                    'projecttaskid' => $passableData['task']->getId(),
                    'customerid' => $passableData['project']->getCustomer()->getId(),
                    'description' => $passableData['description'],
                    'timesheetid' => $aryTimesheetCache['Timesheet']['id']
                ]));
                $this->request->addCommand($command);
                $obj = new TogglImportedRecord($passableData['togglId'], $this->user);
                $this->em->persist($obj);
            }
        }else{
            // this isn't an import from toggl, let's process posted data
            // first let's get the existing time entries
            $aryExistingIDs = [];
            if(count($aryTimesheetCache['Tasks']['daybreakdown']) > 0) {
                foreach ($aryTimesheetCache['Tasks']['daybreakdown'][$day->format('D')]['tasks'] as $objOATask) {
                    $aryExistingIDs[] = $objOATask->id;
                }
            }

            // process posted data
            $PostedIDs = [];
            foreach($aryData as $TaskToUpdate){
                if(!is_null($TaskToUpdate['project'])){ // this will be null if an item was deleted=
                    $command = null;
                    $aryDataType = [
                        'userid' => $this->user->getId(),
                        'date' => new Date(null, null, null, $day->format('m'), $day->format('d'), $day->format('Y')),
                        'decimal_hours' => $TaskToUpdate['time'],
                        'timetypeid' => $TaskToUpdate['timetype']->getId(),
                        'projectid' => $TaskToUpdate['project']->getId(),
                        'projecttaskid' => $TaskToUpdate['task']->getId(),
                        'customerid' => $TaskToUpdate['project']->getCustomer()->getId(),
                        'description' => $TaskToUpdate['description'],
                        'timesheetid' => $aryTimesheetCache['Timesheet']['id']
                    ];
                    if(is_null($TaskToUpdate['id'])){
                        // this is a new task we need to create
                        $command = new Add(['type' => 'Task']);
                    }else{
                        $PostedIDs[] = $TaskToUpdate['id'];
                        $aryDataType['id'] = $TaskToUpdate['id'];
                        $command = new Modify(['type' => 'Task']);
                    }
                    $command->addDataType(new Task($aryDataType));
                    if(!is_null($command)){
                        $this->request->addCommand($command);
                    }
                }
            }

            // now we need to find the record that should be deleted
            // if a record has NOT been posted, it has been deleted.
            // let's take the ids of existing records, and remove those who have been posted
            // that will give us the difference
            $aryIDsToDelete = array_diff($aryExistingIDs, $PostedIDs);

            if(is_array($aryIDsToDelete)){
                foreach($aryIDsToDelete as $idToDelete){
                    $delCommand = new Delete(['type' => 'Task']);
                    $delCommand->addDataType(new Task(['id' => $idToDelete]));
                    $this->request->addCommand($delCommand);
                }
            }
        }

        $this->clearLocalCache();
        $this->container->get('app.event_recorder')->RecordEvent($this->user, UserEventRecorder::UPDATE_OA);
        return $this->getTimesheetDayView($day, true);
    }

    public function getTimeEntryForm(\DateTime $day){
        $builder = $this->container->get('form.factory')->createBuilder('form', null, ['csrf_protection' => false]);
        $timesheetData = $this->getTimesheetDayView($day);

        $timetypes = $this->em->getRepository('SundialBundle:TimeType')->findAll();
        $entityUtility = $this->container->get('app.entity_utility');

        $aryTimeTypes = [];
        foreach($timetypes as $objTimetype){
            $aryTimeTypes[$objTimetype->getId()] = $objTimetype->getType();
        }

        $aryProkectAssignments = $this->em->getRepository('SundialBundle:ProjectAssign')->getAssignedProjects($this->user->getId());
        foreach($aryProkectAssignments as $objAssignment){
            $objProject = $objAssignment->getProject();
            $client = $objAssignment->getCustomer();
            $aryProjects[$client->getId().':'.$objProject->getId()] = $client->getName().' : '.$objProject->getName();
        }

        asort($aryProjects);

        $aryTaskForms = [];
        if(!is_null($timesheetData['Tasks'])){
            foreach($timesheetData['Tasks']['daybreakdown'][$day->format('D')]['tasks'] as $objTask){


                $task = $entityUtility->fetchOrFill('ProjectTask',$objTask->projecttaskid);
                if(is_null($task)){
                    // do something
                }
                $customerId = $task->getCustomer()->getId();
                $projectId = $task->getProject()->getId();
                $key = $customerId.':'.$projectId;
                $label = $task->getCustomer()->getName().' :: '.$task->getProject()->getName();
                if(array_key_exists($label, $timesheetData['BookedHours']['projectbreakdown'])){
                    $booked = $timesheetData['BookedHours']['projectbreakdown'][$label]['booked'];
                    $used = $timesheetData['BookedHours']['projectbreakdown'][$label]['used'];
                    if($used > $booked){
                        $label .= ' (<span class="used overage">'.$used.'</span> of '.$booked.' booked) <span class="glyphicon glyphicon-exclamation-sign"></span>';
                    }elseif($used == $booked){
                        $label .= ' (<span class="used ok">'.$used.'</span> of '.$booked.' booked) <span class="glyphicon glyphicon-ok"></span>';
                    }elseif(($used / $booked)*100 > 90){
                        $label .= ' (<span class="used warning">'.$used.'</span> of '.$booked.' booked) <span class="glyphicon glyphicon-warning-sign"></span>';
                    }else{
                        $label .= ' ('.$used.' of '.$booked.' booked) ';
                    }
                }


                if(!array_key_exists($key, $aryTaskForms)){
                    $aryTaskForms[$key] = ['tasks' => null, 'label' => $label];
                }
                $aryTaskForms[$key]['tasks'][] = $objTask;
            }
        }
        //dump($aryTaskForms);exit;

        $objFormTaskType = $this->container->get('form.task_type');
        foreach($aryTaskForms as $strClientProject => $aryProjectData){
            $arySelectData = [];
            foreach($aryProjectData['tasks'] as $taskObj){
                //dump($taskObj);exit;
                $arySelectData[] = [
                    'id' => intval($taskObj->id),
                    'timetype' => $this->em->getRepository('SundialBundle:TimeType')->find($taskObj->timetypeid),
                    'project' => $this->em->getRepository('SundialBundle:Project')->find($taskObj->projectid),
                    'task' => $this->em->getRepository('SundialBundle:ProjectTask')->find($taskObj->projecttaskid),
                    'time' => floatval($taskObj->decimal_hours),
                    'description' => $taskObj->description,
                    'timesheetId' => $taskObj->timesheetid
                ];
            }

            $builder->add($strClientProject, CollectionType::class, array(
                'entry_type' => $objFormTaskType,
                'allow_add' => false,
                'mapped' => false,
                'data' => $arySelectData,
                'label' => $aryProjectData['label'],
                'entry_options' => ['label' => false],
            ));
        }

        $builder->add('new', CollectionType::class, array(
            'entry_type' => $objFormTaskType,
            'allow_add' => true,
            'mapped' => false,
            'data' => [],
            'label' => false,
            'entry_options' => ['label' => false]
        ));

        $builder->add('newRow', ButtonType::class, [
            'attr' => ['class' => 'btn btn-success addRow'],
            'glyphicon' => 'glyphicon-plus'
        ]);

        $builder->add('saveChanges', SubmitType::class, [
            'attr' => ['class' => 'btn btn-warning submitToOa', 'disabled' => 'disabled'],
            'glyphicon' => 'glyphicon-cloud-upload'
        ]);
        return $builder->getForm();
    }

    public function clearLocalCache(){
        foreach($this->session->all() as $key => $data){
            $match = preg_match('/timesheet\.\d{10,11}/',$key);
            if($match !== false){ // preg_match can returl false on error.  let's be sure we have a case for that
                if($match){
                    $this->session->remove($key);
                }
            }
        }
    }

    public function submitTimesheet($intTimesheetId, $acknowledgeWarnings = false){
        $attributes = ['type' => 'Timesheet'];
        if($acknowledgeWarnings){
            $attributes['submit_warning'] = true;
        }
        $command = new Submit($attributes);
        $command->addDataType(new Timesheet(['id' => $intTimesheetId]));
        $this->request->addCommand($command);
        //$this->request->setDebug(true);
        $response = $this->executeRequest();


        $submitResponse = $response->getCommandResponse('Submit');
        $timesheetData = $submitResponse->getResponseData();
        if($submitResponse->getResponseStatus() == Submit::ERROR_CODE_WARNING){
            throw new SubmitWarningException($timesheetData->warnings);
        }
        if($submitResponse->getResponseStatus() == Submit::ERROR_CODE_ERROR){
            throw new SubmitErrorException($timesheetData->errors);
        }
        if($submitResponse->getResponseStatus() == Command::STATUS_SUCCESS){
            $this->container->get('app.event_recorder')->RecordEvent($this->user, UserEventRecorder::SUBMIT_TIMESHEET);
            if($this->session->has('timesheet.default')){
                if($this->session->get('timesheet.default') == $intTimesheetId){
                    $this->session->remove('timesheet.default');
                }
            }
            return true;
        }
        return false;
    }

    /** HELPER FUNCTIONS **/

    private function _getTasksForWeekCommand($data, $aryOtherTimesheets = null, &$aryCurrentTimesheet = null){
        if($data instanceof \DateTime){ //build the request
            $day = $data;
            // get the week from the day
            $dayBeforeMondayClone = clone $day;
            $dayAfterSundayClone = clone $day;
            $dayBeforeMonday =$dayBeforeMondayClone->modify('Monday this week')->sub(new \DateInterval('P1D'));
            $dayAfterSunday = $dayAfterSundayClone->modify('Sunday this week')->add(new \DateInterval('P1D'));

            //add the day to the read request
            $objReadCommand = new Read([
                'method' => Read::METHOD_EQUAL_TO,
                'type' => 'Task',
                'filters' =>[
                    'filter' => 'newer-than,older-than',
                    'field' => 'date,date',
                ]
            ]);
            $objReadCommand->addDataType(new Date(0,0,0,$dayBeforeMonday->format('n'),$dayBeforeMonday->format('d'),$dayBeforeMonday->format('Y')));
            $objReadCommand->addDataType(new Date(0,0,0,$dayAfterSunday->format('n'),$dayAfterSunday->format('d'),$dayAfterSunday->format('Y')));
            $aryParams = [
                'userid' => $this->user->getId()
            ];
            $objReadCommand->addDataType(new Task($aryParams));

            $this->request->addCommand($objReadCommand);
        }elseif($data instanceof Command){ //process the resposne
            $aryTaskResponseData = null;
            $aryDateData = array();
            $flBillableTotal = 0.00;
            $flNonBillableTotal = 0.00;
            $flAdtlTime = 0.00;


            $intWeekStart = $this->getWeekStart($this->day->format('U'));
            for($i=1;$i<=7;$i++){
                $flBillable = 0.00;
                $flNonBillable = 0.00;
                $intWeekTotalPercent = 0.00;
                $intDayTotalPercent = 0.00;
                $flTotal = 0.00;
                $aryTimeTasks = [];
                // we're going back 2 hours to see what day it is
                // we're going back 2 hours to account for daylight savings
                $intLoopDay =($i*86400)-7200;

                $bSubmited = false;
                $intTimesheetId = null;
                if($data->getResponseStatus() == Command::STATUS_SUCCESS){
                    if(is_null($aryTaskResponseData)){
                        $aryTaskResponseData = $data->getResponseData();
                    }
                    // if there are objects, then let's parse them, otherwise well assume 0
                    foreach($aryTaskResponseData as $objTimeEntry){
                        if(date('D', $intWeekStart+$intLoopDay) == date('D', $objTimeEntry->date)){
                            if($this->session->has('timesheet.default')){
                                if($objTimeEntry->timesheetid != $this->session->get('timesheet.default')){
                                    continue;
                                }
                            }
                            if(
                                (count($aryOtherTimesheets['submitted'])>0 && in_array($objTimeEntry->timesheetid, $aryOtherTimesheets['submitted'])) ||
                                (count($aryOtherTimesheets['approved'])>0 && in_array($objTimeEntry->timesheetid, $aryOtherTimesheets['approved']))
                            ){
                                // there are potentially a lot of edge cases here.
                                // if there's 1 task in a day that's on a subbmitted time sheet
                                // let's assume that the entire day is submitted
                                // we can change this later if needed
                                $bSubmited = true;
                                $flAdtlTime += floatval($objTimeEntry->decimal_hours);
                                if(is_null($intTimesheetId)){
                                    $intTimesheetId = $objTimeEntry->timesheetid;
                                }
                            }
                            $aryTimeTasks[] = $objTimeEntry;
                            if($objTimeEntry->timetypeid == TimeType::BILLABLE){
                                $flBillable += $objTimeEntry->decimal_hours;
                                $flBillableTotal += $objTimeEntry->decimal_hours;
                            }else{
                                $flNonBillable += $objTimeEntry->decimal_hours;
                                $flNonBillableTotal += $objTimeEntry->decimal_hours;
                            }
                        }
                    }
                    $flTotal = $flBillable+$flNonBillable;
                    $intWeekTotalPercent = ($flTotal/40)*100;
                    $intDayTotalPercent = ($flTotal/8)*100;
                }
                $aryTmp = array(
                    'date' => date('Y-m-d', $intWeekStart+$intLoopDay),
                    'billable' => $flBillable,
                    'nonbillable' => $flNonBillable,
                    'total' => $flTotal,
                    'percentOfWeek' => $intWeekTotalPercent,
                    'percentOfDay' => $intDayTotalPercent,
                    'tasks' => $aryTimeTasks,
                    'submitted' => $bSubmited,
                    'timesheetid' => ($bSubmited?$intTimesheetId:$aryCurrentTimesheet['id'])
                );
                $aryDateData[date('D', $intWeekStart+$intLoopDay)] = $aryTmp;
            }

            $aryReturn['daybreakdown'] = $aryDateData;
            $aryReturn['totalBillable'] = $flBillableTotal;
            $aryReturn['totalNonBillable'] = $flNonBillableTotal;
            if(!is_null($aryCurrentTimesheet) && $flAdtlTime > 0){
                $aryCurrentTimesheet['total'] += $flAdtlTime;
            }

            return $aryReturn;

        }
    }

    private function _getOtherTimesheets($data){
        if($data instanceof \DateTime) { //build the request
            // get the week from the day
            $objMonday = new \DateTime(date('Y-m-d H:i:s',$this->getWeekStart($this->day->format('U'))));
            $objStarts = new Date(0, 0, 0, $objMonday->format('n'), $objMonday->format('d'), $objMonday->format('Y'));
            // Submitted Timesheets
            $objReadCommand = new Read(['method' => Read::METHOD_EQUAL_TO,'type' => 'Timesheet']);
            $objReadCommand->addDataType(new Timesheet(['userid' => $this->user->getId(),'limit' => '1','status' => 'S','starts' => $objStarts]));
            $objReadCommand->setReturnValues(['id']);
            $this->request->addCommand($objReadCommand);

            // approved Timesheets
            $objReadCommand = new Read(['method' => Read::METHOD_EQUAL_TO,'type' => 'Timesheet']);
            $objReadCommand->addDataType(new Timesheet(['userid' => $this->user->getId(),'limit' => '1','status' => 'A','starts' => $objStarts]));
            $objReadCommand->setReturnValues(['id']);
            $this->request->addCommand($objReadCommand);

        }elseif(is_array($data)){ //process the resposne
            $objSubmittedCommand = $data[0];
            $objApprovedCommand = $data[1];
            $aryReturn = ['approved' => [], 'submitted' => []];
            if($objSubmittedCommand->getResponseStatus() == Command::STATUS_SUCCESS){
                $aryTimesheetResponseSubmittedData = $objSubmittedCommand->getResponseData();
                foreach($aryTimesheetResponseSubmittedData as $objResponse){
                    $aryReturn['submitted'][] = $objResponse->id;
                }
            }
            if($objApprovedCommand->getResponseStatus() == Command::STATUS_SUCCESS){
                $aryTimesheetResponseApprovedData = $objApprovedCommand->getResponseData();
                foreach($aryTimesheetResponseApprovedData as $objResponse){
                    $aryReturn['approved'][] = $objResponse->id;
                }
            }
            return $aryReturn;
        }
    }

    private function _getTimesheetForWeekCommand($data){
        if($data instanceof \DateTime) { //build the request
            // get the week from the day
            $objMonday = new \DateTime(date('Y-m-d H:i:s',$this->getWeekStart($this->day->format('U'))));

            $objReadCommand = new Read([
                'method' => Read::METHOD_EQUAL_TO,
                'type' => 'Timesheet',
            ]);
            $objReadCommand->addDataType(new Timesheet([
                'userid' => $this->user->getId(),
                'status' => 'O',
                'limit' => '1',
                'starts' => new Date(0, 0, 0, $objMonday->format('n'), $objMonday->format('d'), $objMonday->format('Y'))//$objMonday->format('Y-m-d')
            ]));
            $objReadCommand->setReturnValues(['id', 'status', 'starts', 'ends', 'total', 'default_timetypeid', 'created']);
            $this->request->addCommand($objReadCommand);
        }elseif($data instanceof Command){ //process the resposne
            if($data->getResponseStatus() == Command::STATUS_SUCCESS){
                $aryTimesheetResponseData = $data->getResponseData();
                if(count($aryTimesheetResponseData) > 1){
                    if($this->session->has('timesheet.default')){
                        $intTimesheetDefault = $this->session->get('timesheet.default');
                        $aryValidTimesheet = array_filter($aryTimesheetResponseData, function($a) use ($intTimesheetDefault){
                            return $a->id == $intTimesheetDefault;
                        });
                        $objResponseData = reset($aryValidTimesheet);
                    }else{
                        $objException = new MultipleTimesheetException('Multiple Timesheets Detected');
                        $objException->setTimesheets($aryTimesheetResponseData);
                        throw $objException;
                    }
                }else{
                    if($this->session->has('timesheet.default')){
                        $this->session->remove('timesheet.default');
                    }
                    $objResponseData = $aryTimesheetResponseData[0];
                }

                return ['id' => $objResponseData->id, 'total'=> $objResponseData->total, 'default_timetypeid' => $objResponseData->default_timetypeid];
            }else{
                return null;
            }
        }
    }

    private function _getTimesheetURL($data = null){
        if(is_null($data)) { //build the request
            $objReadCommand = new MakeURL([
                'uid' => $this->user->getId(),
                'page' => 'list-timesheets',
                'app' => 'ta'
            ]);
            $this->request->addCommand($objReadCommand);
        }elseif($data instanceof Command){ //process the resposne
            return $data->getResponseData()[0]->url;
        }
    }

    private function _getRejectedTimeshetsCommand($data = null){
        if(is_null($data)) { //build the request
            $objReadCommand = new Read([
                'method' => Read::METHOD_EQUAL_TO,
                'type' => 'Timesheet',
            ]);
            $objReadCommand->addDataType(new Timesheet([
                'userid' => $this->user->getId(),
                'status' => 'R',
            ]));
            $objReadCommand->setReturnValues(['id']);
            $this->request->addCommand($objReadCommand);
        }elseif($data instanceof Command){ //process the resposne
            if($data->getResponseStatus() == Command::STATUS_SUCCESS){
                return count($data->getResponseData());
            }else{
                return 0;
            }
        }
    }

    private function _getBookedHoursForWeekCommand($data, $taskResponse=null, $timesheetTotal=0){
        if($data instanceof \DateTime) { //build the request
            $day = $data;
            $dayBeforeMondayClone = clone $day;
            $dayAfterSundayClone = clone $day;
            $dayBeforeMonday = $dayBeforeMondayClone->modify('Monday this week');
            $dayAfterSunday = $dayAfterSundayClone->modify('Sunday this week');

            $objReadCommand = new Read([
                'method' => Read::METHOD_EQUAL_TO,
                'type' => 'Booking',
                'filters' => [
                    'filter' => 'date-equal-to,date-equal-to',
                    'field' => 'startdate,enddate',
                ]
            ]);
            $objReadCommand->addDataType(new Date(0,0,0,$dayBeforeMonday->format('n'),$dayBeforeMonday->format('d'),$dayBeforeMonday->format('Y')));
            $objReadCommand->addDataType(new Date(0,0,0,$dayAfterSunday->format('n'),$dayAfterSunday->format('d'),$dayAfterSunday->format('Y')));
            $objReadCommand->addDataType(new Booking([
                'userid' => $this->user->getId()
            ]));
            $objReadCommand->setReturnValues(['hours','projectid','customerid']);
            $this->request->addCommand($objReadCommand);
        }elseif($data instanceof Command){ //process the resposne
            $aryBookedHours = array();
            $aryProjectBreakdown = array('total' => array('booked' => 0.00, 'used' => 0.00));
            $aryCustomerBreakdown = array();
            $aryBookedProjects = array();
            $flBookedHours = 0.0;
            $flBookedHoursWorked = 0.0;
            $projectRepo = $this->em->getRepository('SundialBundle:Project');
            if($data->getResponseStatus() == Command::STATUS_SUCCESS) {
                $aryBookingResponseData = $data->getResponseData();
                //projectbreakdown
                $aryBookedHours = array();
                $aryProjectBreakdown = array('total' => array('booked' => 0.00, 'used' => 0.00));
                $aryCustomerBreakdown = array();
                $aryBookedProjects = array();
                $flBookedHours = 0.0;
                $flBookedHoursWorked = 0.0;
                // "4" is the "Booked hours for the week"
                foreach($aryBookingResponseData as $objBookingRecord){
                    $aryBookedProjects[] = $objBookingRecord->projectid;
                    $flBookedHours += $objBookingRecord->hours;

                    $objProject = $projectRepo->find($objBookingRecord->projectid);
                    $key = $objProject->getCustomer()->getName().' :: '.$objProject->getName();

                    $aryBookedHours[$key] = $objBookingRecord->hours;

                    //let's build the project breakdown
                    if(!array_key_exists($key, $aryProjectBreakdown)){
                        $aryProjectBreakdown[$key] = array('booked' => 0.00, 'used' => 0.00, 'workedPercent' => 0.00, 'overPercent' => 0.00);
                    }
                    $aryProjectBreakdown[$key]['booked'] += $objBookingRecord->hours;
                    $aryProjectBreakdown['total']['booked'] += $objBookingRecord->hours;
                }


            }

            // let's use the result from "2" to build the remainder of the project breakdown
            // this is for items that we worked, but wern't booked for
            if($taskResponse->getResponseStatus() == Command::STATUS_SUCCESS) {
                $aryTaskResponse = $taskResponse->getResponseData();
                if(!is_null($aryTaskResponse)){

                    foreach($aryTaskResponse as $objTimeEntry){
                        //dump($objTimeEntry);exit;
                        $objProject = $projectRepo->find($objTimeEntry->projectid);

                        $key = $objProject->getCustomer()->getName().' :: '.$objProject->getName();

                        if(!array_key_exists($key, $aryProjectBreakdown)){
                            $aryProjectBreakdown[$key] = [
                                'booked' => 0.00,
                                'used' => 0.00,
                                'workedPercent' => 0.00,
                                'overPercent' => 0.00,
                                'projectid' => $objTimeEntry->projectid
                            ];
                        }
                        $aryProjectBreakdown[$key]['used'] += $objTimeEntry->decimal_hours;

                        if($aryProjectBreakdown[$key]['booked'] > 0){
                            $aryProjectBreakdown[$key]['workedPercent'] = ($aryProjectBreakdown[$key]['used'] / $aryProjectBreakdown[$key]['booked'])*100;
                            if($aryProjectBreakdown[$key]['used'] > $aryProjectBreakdown[$key]['booked']){
                                $flOverage = (($aryProjectBreakdown[$key]['used'] - $aryProjectBreakdown[$key]['booked'])/$aryProjectBreakdown[$key]['used'])*100;
                                $aryProjectBreakdown[$key]['workedPercent'] = 100-$flOverage;
                                $aryProjectBreakdown[$key]['overPercent'] = $flOverage;
                                $aryProjectBreakdown[$key]['overText'] = $aryProjectBreakdown[$key]['used'] - $aryProjectBreakdown[$key]['booked'].'hr over';
                            }
                        }else{
                            $aryProjectBreakdown[$key]['workedPercent'] = 0.00;
                            $aryProjectBreakdown[$key]['overPercent'] = 100.00;
                            $aryProjectBreakdown[$key]['overText'] = $aryProjectBreakdown[$key]['used'].'hr over';
                        }

                        $aryProjectBreakdown[$key]['work'][$objTimeEntry->date][] = [
                            'desc' => $objTimeEntry->description,
                            'time' => $objTimeEntry->decimal_hours
                        ];

                        $aryProjectBreakdown['total']['used'] += $objTimeEntry->decimal_hours;

                        if(array_key_exists($key, $aryBookedHours)){
                            $flBookedHoursWorked += $objTimeEntry->decimal_hours;
                        }
                    }
                }
            }


            //override the defaults
            $aryReturn['bookedHoursData'] = $aryBookedHours;
            $aryReturn['bookedHours'] = $flBookedHours;
            $aryReturn['bookedHoursWorked'] = $flBookedHoursWorked;
            $aryReturn['projectbreakdown'] = $aryProjectBreakdown;
            //$aryReturn['customerbreakdown'] = $aryCustomerBreakdown;
            $aryReturn['bookedprojects'] = $aryBookedProjects;
            return $aryReturn;
        }
    }

    private function _isTogglLinked(){
        $objTogglKeyRepo = $this->em->getRepository('SundialBundle:TogglKey');
        $objKey = $objTogglKeyRepo->findOneByUser($this->user);
        return (!is_null($objKey));
    }

    private function getWeekStart($intRequestedDay = false){
        if($intRequestedDay){
            if(date('D', $intRequestedDay) == "Mon"){
                return strtotime(date("Y-m-d", $intRequestedDay));
            }else{
                return strtotime('last Monday', $intRequestedDay);
            }
        }else{
            if(date('D') == "Mon"){
                return strtotime(date("Y-m-d"));
            }else{
                return strtotime('last Monday');
            }
        }
    }

    // this function also resides in the TogglService.
    private function _convertSecondsToDecimal($intSeconds){
        if($intSeconds < 0){ //this denotes that the timer is running.  let's take "NOW" as the end time
            return false;
        }
        $intRoundThreshold = 360; //6 min
        $intIncrement = 900; //15 min
        $divider = 3600;

        if($intSeconds < $intRoundThreshold){
            $newCalc = $intIncrement;
        }else{
            $intMod = ($intSeconds % $intIncrement);
            $calc = $intSeconds - $intMod;
            if($intMod >= $intRoundThreshold){
                $newCalc = ($calc + $intIncrement);
            }else{
                $newCalc = $calc;
            }
        }
        return $newCalc / $divider;
    }
}