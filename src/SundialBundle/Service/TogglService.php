<?php

namespace SundialBundle\Service;


use SundialBundle\Entity\Project;
use SundialBundle\Entity\ProjectTask;
use SundialBundle\Entity\TimeType;
use SundialBundle\Entity\TogglProjectSetting;
use SundialBundle\Entity\User;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilder;

class TogglService
{
    private $container;
    private $user;
    private $apiUrl = 'https://www.toggl.com/api/v8/';
    private $session;

    function __construct(Container $container)
    {
        $this->container = $container;
        $this->user = $this->container->get('security.token_storage')->getToken()->getUser();
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->session = $this->container->get('session');
    }

    public function attemptAuth($user, $pass){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERPWD, "$user:$pass");

        $strProjectsURL = $this->apiUrl."me";
        curl_setopt($ch,CURLOPT_URL,$strProjectsURL);
        return $this->__callToggl($ch);
    }

    public function getClientsAndProjects(){
        $objTogglKeyRepo = $this->em->getRepository('SundialBundle:TogglKey');
        $objKey = $objTogglKeyRepo->findOneByUser($this->user);
        if($objKey){
            $aryReturn = [];

            // let's start the curl call
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_USERPWD, $objKey->getToken().":api_token");

            // projects
            $strProjectsURL = $this->apiUrl."workspaces/".$objKey->getWorkspaceId()."/projects";
            curl_setopt($ch,CURLOPT_URL,$strProjectsURL);
            $aryReturn['projects'] = $this->__callToggl($ch);

	        $ch = curl_init();
            curl_setopt($ch, CURLOPT_USERPWD, $objKey->getToken().":api_token");

            // clients
            $strClientsURL = $this->apiUrl."workspaces/".$objKey->getWorkspaceId()."/clients";
            curl_setopt($ch,CURLOPT_URL,$strClientsURL);
            $aryReturn['clients'] = $this->__callToggl($ch);
            // let's execute
            return $aryReturn;
        }
        return false;
    }

    public function getSettingsForm(FormBuilder $builder){
        $objTogglProjectSettingRepo = $this->em->getRepository('SundialBundle:TogglProjectSetting');
        $aryTogglProjects = $this->getClientsAndProjects();

        if($aryTogglProjects === false){
            return false;
        }

        if($aryTogglProjects['projects']['info']['http_code'] == 200){
            $aryProjects = json_decode($aryTogglProjects['projects']['result'], true);
        }

        if($aryTogglProjects['clients']['info']['http_code'] == 200){
            $aryClients = json_decode($aryTogglProjects['clients']['result'], true);
        }

        // let's build an array to display
        $aryDisplay = [0 => ['name' => 'no client', 'projects' => []]];
        foreach($aryClients as $aryClient){
            if(!array_key_exists($aryClient['id'], $aryDisplay)){
                $aryDisplay[$aryClient['id']] = ['name' => $aryClient['name'], 'projects' => []];
            }
            foreach($aryProjects as $aryProject){
                $key = 0;
                if(array_key_exists('cid', $aryProject)){
                    if($aryProject['cid'] != $aryClient['id']){
                        continue;
                    }
                    $key = $aryProject['cid'];
                }
                if(!array_key_exists($aryProject['id'], $aryDisplay[$key]['projects'])){
                    $aryDisplay[$key]['projects'][$aryProject['id']] = $aryProject;
                }
            }
        }

        $aryNewFormData = [];
        $aryExistingFormData = [];
        foreach($aryDisplay as $clientID => $aryClientData){
            foreach($aryClientData['projects'] as $projectID => $aryProjectData){
                $objSetting = $objTogglProjectSettingRepo->findOneBy([
                    'user' => $this->user,
                    'togglProjectId' => $aryProjectData['id']
                ]);
                if($objSetting){
                    $aryExistingFormData[] = [
                        'id' => $objSetting->getId(),
                        'togglClient' => $aryClientData['name'],
                        'togglProject' => $aryProjectData['name'],
                        'togglProjectId' => $objSetting->getTogglProjectId(),
                        'timetype' => $objSetting->getTimetype(),
                        'project' => $objSetting->getProject(),
                        'task' => $objSetting->getTask(),
                    ];
                }else{
                    $aryNewFormData[] = [
                        'id' => null,
                        'togglClient' => $aryClientData['name'],
                        'togglProject' => $aryProjectData['name'],
                        'togglProjectId' => $aryProjectData['id'],
                        'timetype' => null,
                        'project' => null,
                        'task' => null,
                    ];
                }
            }
        }

        $builder->add('existing', CollectionType::class, [
            //'label' => $aryClientData['name'].': '.$aryProjectData['name'],
            'label' => false,
            'data' => $aryExistingFormData,
            'entry_type' => $this->container->get('form.toggl_settings_type'),
            'entry_options' => ['label' => false]
        ]);

        $builder->add('new', CollectionType::class, [
            //'label' => $aryClientData['name'].': '.$aryProjectData['name'],
            'label' => false,
            'data' => $aryNewFormData,
            'entry_type' => $this->container->get('form.toggl_settings_type'),
            'entry_options' => ['label' => false]
        ]);

        $builder->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary btn-block'],
            'label' => 'Save'
        ]);
        return $builder->getForm();
    }

    public function updateCreateSetting(TimeType $timetype, Project $project, ProjectTask $task, User $user, $intTogglProjectId){
        $objPorjectSettingsRepo = $this->em->getRepository('SundialBundle:TogglProjectSetting');
        $obj = $objPorjectSettingsRepo->findOneBy([
            'user' => $user,
            'togglProjectId' => $intTogglProjectId
        ]);
        if(is_null($obj)){
            $obj = new TogglProjectSetting();
        }
        $obj->setTimetype($timetype);
        $obj->setCustomer($project->getCustomer());
        $obj->setProject($project);
        $obj->setTask($task);
        $obj->setUser($user);
        $obj->setTogglProjectId($intTogglProjectId);
        $this->em->persist($obj);
        $this->em->flush();
    }
    
    public function removeSetting(User $user, $intTogglProjectId){
        $objPorjectSettingsRepo = $this->em->getRepository('SundialBundle:TogglProjectSetting');
        $obj = $objPorjectSettingsRepo->findOneBy([
            'user' => $user,
            'togglProjectId' => $intTogglProjectId
        ]);
        if($obj){
            $this->em->remove($obj);
            $this->em->flush();
        }
    }

    private function __callToggl($ch){
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return ['result' => $output, 'info' => $info];
    }

    public function getTimeRecords(\DateTime $day){
        // let's get the user's token
        $objTogglKeyRepo = $this->em->getRepository('SundialBundle:TogglKey');
        $objKey = $objTogglKeyRepo->findOneByUser($this->user);
        if($objKey){
            // the user has a token,
            // let's set some parameters
            $aryParameters = [];
            $aryParameters['start_date'] = $day->format("c");
            $aryParameters['end_date'] = $day->add(new \DateInterval('PT23HPT59MPT59S'))->format("c");

            // let's start the curl call
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_USERPWD, $objKey->getToken().":api_token");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            $strURL = $this->apiUrl."time_entries?".http_build_query($aryParameters);
            curl_setopt($ch,CURLOPT_URL,$strURL);

            // let's execute
            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            return ['result' => $output, 'info' => $info];
        }
        return false;
    }

    public function getForm(FormBuilder $builder){

        $objSession = $this->container->get('session');
        $aryProjectResult = $this->getClientsAndProjects();

        if(array_key_exists('projects', $aryProjectResult) && $aryProjectResult['projects']['info']['http_code'] == 200) {
            $aryProjects = json_decode($aryProjectResult['projects']['result'], true);
        }
        if(array_key_exists('clients', $aryProjectResult) && $aryProjectResult['clients']['info']['http_code'] == 200) {
            $aryClients = json_decode($aryProjectResult['clients']['result'], true);
        }
        $aryData = [];
        if($objSession->has('toggltime')){
            $objTogglSettings = $this->em->getRepository('SundialBundle:TogglProjectSetting');
            $aryPreProcessedData = $objSession->get('toggltime');
            foreach($aryPreProcessedData as $togglData){
                $aryTmp = [
                    'id' => null,
                    'togglId' => $togglData['id'],
                    'togglProjectId' => (array_key_exists('pid', $togglData)?$togglData['pid']:null),
                    'togglTime' => $togglData['duration'],
                    'timetype' => null,
                    'project' => null,
                    'task' => null,
                    'time' => $this->_convertSecondsToDecimal($togglData['duration']),
                    'description' => $togglData['description'],
                    'mapped' => false,
                    'togglProjectName' => null,
                ];
                if(!is_null($aryTmp['togglProjectId'])){
                    $objSetting = $objTogglSettings->findOneBy([
                        'user' => $this->user,
                        'togglProjectId' => $togglData['pid']
                    ]);
                    $aryTmp['mapped'] = boolval($objSetting);
                    if(!is_null($objSetting)){
                        $aryTmp['timetype'] = $objSetting->getTimeType();
                        $aryTmp['project'] = $objSetting->getProject();
                        $aryTmp['task'] = $objSetting->getTask();
                    }
                    if(isset($aryProjects)){
                        // get the project
                        $aryProject = array_filter($aryProjects, function($ary) use ($aryTmp){
                            return $ary['id'] == $aryTmp['togglProjectId'];
                        });
                        $aryProject = reset($aryProject);
                        $aryTmp['togglProjectName'] = $aryProject['name'];

                        // there's a client on this project, let's get that too
                        if(array_key_exists('cid', $aryProject) && isset($aryClients)){
                            $aryClient = array_filter($aryClients, function($ary) use ($aryProject){
                                return $ary['id'] == $aryProject['cid'];
                            });
                            $aryClient = reset($aryClient);
                            $aryTmp['togglProjectName'] .= ' :: '.$aryClient['name'];
                        }
                    }
                }
                $aryData[] = $aryTmp;
            }
        }
        $builder->add('timeToImport', CollectionType::class, array(
            'entry_type' => $this->container->get('form.task_type'),
            'allow_add' => false,
            'data' => $aryData,
            'entry_options' => ['label' => false],
        ));
        $builder->add('submit', SubmitType::class, array(
            'attr' => ['class' => 'btn btn-primary btn-success btn-accept-records'],
            'label' => 'Accept these Time Records',
            'glyphicon' => 'glyphicon-ok-sign'
        ));
        //dump($aryData);exit;
        return $builder->getForm();
    }

    // this function also resides in the TimesheetService
    private function _convertSecondsToDecimal($intSeconds){
        if($intSeconds < 0){ //this denotes that the timer is running.  let's take "NOW" as the end time
            return false;
        }
        $intRoundThreshold = 360; //6 min
        $intIncrement = 900; //15 min
        $divider = 3600;

        if($intSeconds < $intRoundThreshold){
            $newCalc = $intIncrement;
        }else{
            $intMod = ($intSeconds % $intIncrement);
            $calc = $intSeconds - $intMod;
            if($intMod >= $intRoundThreshold){
                $newCalc = ($calc + $intIncrement);
            }else{
                $newCalc = $calc;
            }
        }
        return $newCalc / $divider;
    }
}
