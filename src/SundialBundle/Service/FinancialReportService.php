<?php

namespace SundialBundle\Service;


use OpenAir\Base\Command;
use OpenAir\Commands\Read;
use OpenAir\DataTypes\Projectbillingrule;
use OpenAir\DataTypes\RateCardItem;
use OpenAir\DataTypes\Task;
use OpenAir\DataTypes\Uprate;
use OpenAir\Response;
use SundialBundle\Entity\Booking;
use SundialBundle\Entity\Budget;
use SundialBundle\Entity\FinancialReport;
use SundialBundle\Entity\Project;
use SundialBundle\Entity\Rate;
use SundialBundle\Form\BudgetType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilder;

class FinancialReportService extends OpenAirService
{
    private $report;

    function __construct(Container $container){
        parent::__construct($container);
    }

    public function runReport(FinancialReport $report){
        $this->report = $report;
        $this->_getTasksForProject($report->getProject()); //0
        $this->_getBillingRules(); //1
        $this->_getBookingForProject(); //2
        //$this->request->setDebug(true);
        $response = $this->executeRequest();
        $bReturn = false;
        if($response instanceof Response){
            $aryReadRequests = $response->getCommandResponse('Read');
            $this->_getTasksForProject($aryReadRequests[0]);
            $this->_getBillingRules($aryReadRequests[1]); //1
            $this->_getBookingForProject($aryReadRequests[2]);
            $bReturn = true;
        }
        if($bReturn){
            $runDate = new \DateTime();
            $report->setLastRun($runDate);
            $this->em->persist($report);
            $this->em->flush();
            return $runDate;
        }
        return $bReturn;
    }

    /**
     * @param $reportId
     * @return array|bool
     * @throws \Throwable
     */
    public function getReportView($reportId){
        $reportRepo = $this->em->getRepository('SundialBundle:FinancialReport');
        $objReport = $reportRepo->find($reportId);
        if(is_null($objReport)){
            return false;
        }
        $taskRepo = $this->em->getRepository('SundialBundle:Task');
        $timeTypeRepo = $this->em->getRepository('SundialBundle:TimeType');
        $budgetRepo = $this->em->getRepository('SundialBundle:Budget');
        $rateRepo = $this->em->getRepository('SundialBundle:Rate');

        $reportForm = $this->container->get('form.factory')->create('financial_report_type', $objReport);
        $reportForm->remove('customer');
        $reportForm->remove('project');
        return [
            'ratesExist' => (count($rateRepo->findByReport($objReport)) > 0),
            'report' => $reportRepo->find($reportId),
            'timetypes' => $timeTypeRepo->findAll(),
            'timeBreakdown' => $taskRepo->getTimeBreakdown($objReport),
            'rawData' => $taskRepo->findByReport($objReport),
            'dataByTask' => $taskRepo->dataByTask($objReport),
            'tasksForMonth' => $taskRepo->dataByTask($objReport, date("m")),
            'dataByUser' => $taskRepo->dataByUser($objReport),
            'budgetVsActuals' => $budgetRepo->budgetVsActuals($objReport),
            'budgetForm' => $this->getBudgetForm($objReport),
            'reportForm' => $reportForm,
            'billableVsNonbill' => $taskRepo->billableAndNonbillable($objReport),
        ];
    }

    public function getBudgetForm(FinancialReport $report){
        $objProjectTaskRepo = $this->em->getRepository('SundialBundle:ProjectTask');
        $objBudgetRepo = $this->em->getRepository('SundialBundle:Budget');
        $aryData = $objProjectTaskRepo->getTasksForProject($report->getProject()->getId(), true);
        $builder = $this->container->get('form.factory')->createBuilder('form');

        foreach($aryData as $id => $label){
            $objBudget = $objBudgetRepo->findOneBy([
                'report' => $report,
                'task' => $objProjectTaskRepo->find($id)
            ]);

            if(is_null($objBudget)){
                $objBudget = new Budget();
                $objBudget->setReport($report);
                $objBudget->setTask($objProjectTaskRepo->find($id));
            }

            $builder->add($id, CollectionType::class, [
                'entry_type' => BudgetType::class,
                'entry_options' => ['label' => false],
                'label' => $label,
                'data' => [$objBudget]
            ]);
        }
        $builder->add('submit', SubmitType::class, [
            'attr'=>['class'=>'btn btn-primary btn-block', 'style'=>'margin-top:15px;']
        ]);
        return $builder->getForm();
    }

    /** HELPER FUNCTIONS **/

    private function _getBookingForProject($data=null){
        if(is_null($data)){
            $readCommand = new Read(['type' => 'Booking', 'method' => Read::METHOD_EQUAL_TO]);
            $dataType = new \OpenAir\DataTypes\Booking(['projectid' => $this->report->getProject()->getId()]);
            $readCommand->addDataType($dataType);
            $this->request->addCommand($readCommand);
        }elseif($data instanceof Command){
            if($data->getResponseStatus() == Command::STATUS_SUCCESS){
                $bookingRepo = $this->em->getRepository('SundialBundle:Booking');
                $entityUtility = $this->container->get('app.entity_utility');
                $aryBookings = $bookingRepo->findByReport($this->report);
                foreach($aryBookings as $objBooking){
                    $this->em->remove($objBooking);
                }
                $this->em->flush();
                foreach($data->getResponseData() as $aryOaData){
                    $objBooking = new Booking();
                    $objBooking->setUser($entityUtility->fetchOrFill('User', $aryOaData->userid));
                    $objBooking->setCustomer($entityUtility->fetchOrFill('Customer', $aryOaData->customerid));
                    $objBooking->setHours($aryOaData->hours);
                    $objBooking->setReport($this->report);
                    $objBooking->setId($aryOaData->id);
                    $objBooking->setProject($entityUtility->fetchOrFill('Project', $aryOaData->projectid));
                    $this->em->persist($objBooking);
                }
                $this->em->flush();
            }
        }
    }

    private function _getBillingRules($data=null){
        if(is_null($data)){
            $readCommand = new Read(['type' => 'Projectbillingrule', 'method' => Read::METHOD_EQUAL_TO]);
            $dataType = new Projectbillingrule(['projectid' => $this->report->getProject()->getId(), 'active' => true]);
            $readCommand->addDataType($dataType);
            $this->request->addCommand($readCommand);
        }elseif($data instanceof Command){
            if($data->getResponseStatus() == Command::STATUS_SUCCESS){
                $this->_processRateCardsFromOpenAir($data->getResponseData());
            }
        }
    }

    private function _getTasksForProject($data){
        if($data instanceof Project){ //build the request
            $readCommand = new Read(['type' => 'Task', 'method' => Read::METHOD_EQUAL_TO]);
            $dataType = new Task([
                'projectid' => $data->getId()
            ]);
            $readCommand->addDataType($dataType);
            $this->request->addCommand($readCommand);
        }elseif($data instanceof Command){
            if($data->getResponseStatus() == Command::STATUS_SUCCESS || $data->getResponseStatus() == Read::ERROR_ID_CODE_INVALID){ //ERROR_ID_CODE_INVALID == no results
                $this->_bulkStoreTasksFromOpenAir($data->getResponseData());
                return true;
            }
            return false;
        }
    }

    private function _processRateCardsFromOpenAir(array $aryOAData){
        // its ok to use this->request because it's already done being used above
        if(count($aryOAData) > 0){
            foreach($aryOAData as $objRateCard){
                switch($objRateCard->rate_from){
                    case 'U':
                        // this is an override rate for a single user.
                        // we need to get this rate by reading from uprate
                        // with the project and user id
                        $aryUsers = explode(',', $objRateCard->user_filter);
                        foreach($aryUsers as $intUserID){
                            $read = new Read(['type' => 'Uprate', 'method' => Read::METHOD_EQUAL_TO]);
                            $Uprate = new Uprate(['projectid' => $objRateCard->projectid, 'userid' => $intUserID]);
                            $read->addDataType($Uprate);
                            $this->request->addCommand($read);
                        }
                        break;
                    case 'R':
                        // the billing rule is from a rate card.
                        // we need to pull the rate card items and create user rates
                        $read = new Read(['type' => 'RateCardItem', 'method' => Read::METHOD_EQUAL_TO]);
                        $RateCardItem = new RateCardItem(['rate_card_id' => $objRateCard->rate_cardid]);
                        $read->addDataType($RateCardItem);
                        $this->request->addCommand($read);
                }
            }
            $response = $this->executeRequest();
            if($response instanceof Response){
                $userRepo = $this->em->getRepository('SundialBundle:User');
                $rateRepo = $this->em->getRepository('SundialBundle:Rate');
                $aryReadResponses = $response->getCommandResponse('Read');
                foreach($aryReadResponses as $aryReadResponse){
                    if($aryReadResponse->getResponseStatus() == Command::STATUS_SUCCESS){
                        foreach($aryReadResponse->getResponseData() as $objResponse){
                            if($objResponse instanceof RateCardItem){
                                $aryUsers = $userRepo->findByJobcode($objResponse->job_code_id);
                                if(!is_null($aryUsers)){
                                    foreach($aryUsers as $objUser){
                                        $rate = $rateRepo->findOneBy(['user' => $objUser, 'report' => $this->report]);
                                        if(is_null($rate)){
                                            $rate = new Rate();
                                            $rate->setUser($objUser);
                                        }
                                        $rate->setReport($this->report);
                                        $rate->setRate($objResponse->rate);
                                        if(!empty($objResponse->start)){
                                            $rate->setStart(new \DateTime(date('Y-m-d H:i:s',$objResponse->start)));
                                        }
                                        if(!empty($this->end)){
                                            $rate->setEnd(new \DateTime(date('Y-m-d H:i:s',$objResponse->end)));
                                        }
                                        $this->em->persist($rate);
                                        $this->em->flush();
                                    }
                                }
                            }elseif($objResponse instanceof Uprate){
                                $objUser = $userRepo->find($objResponse->userid);
                                $rate = $rateRepo->findOneBy([
                                    'user' => $objUser,
                                    'report' => $this->report
                                ]);
                                if(is_null($rate)){
                                    $rate = new Rate();
                                    $rate->setUser($objUser);
                                }
                                $rate->setReport($this->report);
                                $rate->setOverriddenRate($objResponse->rate);
                                $this->em->persist($rate);
                                $this->em->flush();
                            }
                        }
                    }
                }
            }
        }
    }

    private function _bulkStoreTasksFromOpenAir(array $aryOAData){
        $taskRepo = $this->em->getRepository('SundialBundle:Task');
        $aryDeleteTasks = $taskRepo->findBy([
            'report' => $this->report
        ]);
        foreach($aryDeleteTasks as $objTask){
            $this->em->remove($objTask);
        }
        $this->em->flush();

        if(count($aryOAData) > 0){
            //let's refill
            $timetypeRepo = $this->em->getRepository('SundialBundle:TimeType');
            $projecttaskRepo = $this->em->getRepository('SundialBundle:ProjectTask');
            $entityUtitlity = $this->container->get('app.entity_utility');
            foreach($aryOAData as $objOATask){
                $objTask = new \SundialBundle\Entity\Task();
                $objUser = $entityUtitlity->fetchOrFill('User', $objOATask->userid);
                $objTask->setUser($objUser);
                $objTask->setId($objOATask->id);
                $objTask->setDate(new \DateTime(date('Y-m-d H:i:s', $objOATask->date)));
                $objTask->setDecimalHours($objOATask->decimal_hours);
                $objTask->setDescription($objOATask->description);
                $objTask->setReport($this->report);
                $objTask->setProjectTask($projecttaskRepo->find($objOATask->projecttaskid));
                $objTask->setTimeType($timetypeRepo->find($objOATask->timetypeid));
                $this->em->persist($objTask);
            }
            $this->em->flush();
        }
        return true;
    }

    /*private function createFormBuilder($data = null, array $options = array())
    {
        if (method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')) {
            $type = 'Symfony\Component\Form\Extension\Core\Type\FormType';
        } else {
            // not using the class name is deprecated since Symfony 2.8 and
            // is only used for backwards compatibility with older versions
            // of the Form component
            $type = 'form';
        }

        return $this->container->get('form.factory')->createBuilder($type, $data, $options);
    }*/

}
