<?php

namespace SundialBundle\Service;


use Doctrine\Common\Annotations\AnnotationReader;
use OpenAir\Base\Command;
use OpenAir\Commands\Read;
use OpenAir\DataTypes\Projecttask;
use OpenAir\Response;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Process\Exception\LogicException;

class EntityUtility extends OpenAirService
{

    function __construct(Container $container)
    {
        parent::__construct($container);
    }

    public function fillTasksForProject($intProjectID){
        $projectTaskRepo = $this->em->getRepository('SundialBundle:ProjectTask');
        $objRead = new Read([
            'method' => Read::METHOD_EQUAL_TO,
            'type' => 'Projecttask'
        ]);

        $objDatatype = new Projecttask();
        $objDatatype->projectid = $intProjectID;

        $aryFieldMaps = $this->__buildOAFields('SundialBundle\\Entity\\ProjectTask');
        $objRead->addDataType($objDatatype);
        $objRead->setReturnValues(array_keys($aryFieldMaps));

        $this->request->addCommand($objRead);
        //$this->request->setDebug(true);
        $objResponse = $this->executeRequest();

        $aryExistingEntities = $projectTaskRepo->findByProject($intProjectID);
        $aryProjectIDs = [];
        foreach($aryExistingEntities as $objExistingEntity){
            $aryProjectIDs[] = $objExistingEntity->getId();
        }

        if($objResponse instanceof Response) {
            // get the read response
            $aryReads = $objResponse->getCommandResponse('Read');
            $objRead = $aryReads[0]; // there's only 1
            $intResultCode =$objRead->getResponseStatus();

            // check the result
            if($intResultCode == Command::STATUS_SUCCESS){
                $aryResponseData = $objRead->getResponseData();
                foreach ($aryResponseData as $data) {
                    if(in_array($data->id, $aryProjectIDs))continue; //don't add it if we already have it
                    $sundialEntity = new \SundialBundle\Entity\ProjectTask();

                    foreach ($aryFieldMaps as $oaField => $arySundialFieldData) {
                        if (is_null($arySundialFieldData['target'])) {
                            $val = $data->$oaField;
                        } else {
                            $secondRepo = $this->em->getRepository('SundialBundle:' . $arySundialFieldData['target']);
                            $val = $secondRepo->find($data->$oaField);
                            if (!$val) {
                                $val = $this->container->get('app.entity_utility')->fetchOrFill($arySundialFieldData['target'], $data->$oaField);
                            }
                        }
                        $strSetFunction = 'set' . ucfirst($arySundialFieldData['sundialField']);
                        $sundialEntity->$strSetFunction($val);
                    }
                    $this->em->persist($sundialEntity);
                }
            }

        }
        $this->em->flush();
        return $projectTaskRepo->getTasksForProject($intProjectID);
    }
    
    public function fetchOrFill($entity, $id){

        $UCentity = ucfirst(strtolower($entity));
        $RepoName = 'SundialBundle:'.$entity;
        $entityClass = 'SundialBundle\\Entity\\'.$entity;
        $aryFieldMaps = $this->__buildOAFields($entityClass);
        $objRepo = $this->em->getRepository($RepoName);
        if($id == ''){
            return null;
        }
        if($objRepo){

            $obj = $objRepo->find($id);

            if($obj){
                return $obj;
            }else{
                $objRead = new Read([
                    'method' => Read::METHOD_EQUAL_TO,
                    'type' => $UCentity
                ]);
                $datatypeClass = '\\OpenAir\\DataTypes\\'.$UCentity;
                $objDataType = new $datatypeClass();
                $objDataType->id = $id;
                $objRead->addDataType($objDataType);
                $objRead->setReturnValues(array_keys($aryFieldMaps));
                $this->request->addCommand($objRead);
                //$this->request->setDebug(true);
                $objResponse = $this->executeRequest();
                if($objResponse instanceof Response){
                    $aryReads = $objResponse->getCommandResponse('Read');
                    $objRead = $aryReads[0]; // there's only 1
                    $intResultCode =$objRead->getResponseStatus();
                    if($intResultCode == Command::STATUS_SUCCESS){
                        $aryResponseData = $objRead->getResponseData();
                        foreach($aryResponseData as $data){ // there should be only 1 response
                            $sundialEntity = new $entityClass();
                            foreach($aryFieldMaps as $oaField => $arySundialFieldData){
                                if(is_null($arySundialFieldData['target'])){
                                    $val = $data->$oaField;
                                }else{
                                    $secondRepo = $this->em->getRepository('SundialBundle:'.$arySundialFieldData['target']);
                                    $val = $secondRepo->find($data->$oaField);
                                    if(!$val){
                                        $val = $this->container->get('app.entity_utility')->fetchOrFill($arySundialFieldData['target'], $data->$oaField);
                                    }
                                }
                                $strSetFunction = 'set'.ucfirst($arySundialFieldData['sundialField']);
                                $sundialEntity->$strSetFunction($val);
                            }
                            $this->em->persist($sundialEntity);
                        }

                        $this->em->flush();
                        return $sundialEntity;
                    }elseif($intResultCode == Read::ERROR_ID_CODE_INVALID){
                        // not found
                        return null;
                    }
                }else{
                    throw new \Error('Unable to fetch from Open Air');
                }
            }
        }else{
            throw new LogicException("Missing Entity ".$RepoName);
        }
    }
    
    private function __buildOAFields($entityClass){
        $reader = new AnnotationReader();
        $aryMetadata = $this->em->getClassMetadata($entityClass);
        $aryReturn = [];
        foreach($aryMetadata->getReflectionProperties() as $fieldName => $objReflectionClass){
            $oaField = $reader->getPropertyAnnotation($objReflectionClass, 'SundialBundle\Annotations\OpenAirField');

            if(!is_null($oaField)){
                $aryReturn[$oaField->value] = ['sundialField' => $fieldName, 'target' => $oaField->target, 'type' => $oaField->type];
            }
        }
        return $aryReturn;
    }

}
