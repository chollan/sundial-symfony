<?php

namespace SundialBundle\Service;


use Doctrine\ORM\EntityManager;
use SundialBundle\Entity\User;
use SundialBundle\Entity\UserEventRecord;

class UserEventRecorder
{
    private $em;
    const LOGIN = 'Login';
    const IMPORT = 'Toggl Import';
    const LOGOUT = 'Logout';
    const UPDATE_OA = 'Update OA Data';
    const CREATE_TIMESHEET = 'Create Timesheet';
    const SUBMIT_TIMESHEET = 'Submit Timesheet';
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function RecordEvent(User $user, $event){
        $this->em->persist(
            new UserEventRecord($user, $event)
        );
        $this->em->flush();
    }

}