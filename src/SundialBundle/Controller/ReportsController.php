<?php

namespace SundialBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SundialBundle\Entity\Budget;
use SundialBundle\Entity\FinancialReport;
use SundialBundle\Form\BudgetType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


/**
 * Class ReportsController
 * @package SundialBundle\Controller
 * @Route("/reports")
 */
class ReportsController extends Controller
{

    /**
     * @Route("/", name="reports_dashboard")
     * @Security("has_role('ROLE_USER')")
     */
    public function dashboardAction(){

        return $this->render('SundialBundle:reports:dashboard.html.twig',[
            'reports' => $this->getDoctrine()->getRepository('SundialBundle:FinancialReport')->findBy(['user' => $this->getUser()])
        ]);
    }

    /**
     * @Route("/add", name="add_report")
     */
    public function addReportAction(Request $request){
        $projectRepo = $this->getDoctrine()->getRepository('SundialBundle:Project');
        $objReport = new FinancialReport();
        $objReport->setUser($this->getUser());
        $form = $this->createForm('financial_report_type', $objReport);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $objReport = $form->getData();
            $objReport->setProject($projectRepo->find($objReport->getProject()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($objReport);
            $em->flush();
            $this->addFlash('success', 'Your report was created.  Please enter your budget information below.');
            return $this->redirectToRoute('report_budget', ['id'=>$objReport->getId()]);
        }
        return $this->render('SundialBundle:reports:add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/budgets/{id}", name="report_budget", requirements={"id":"\d+"})
     */
    public function setBudgetAction(Request $request, $id){
        $objReportRepo = $this->getDoctrine()->getRepository('SundialBundle:FinancialReport');
        $report = $objReportRepo->find($id);
        $this->denyAccessUnlessGranted('edit', $report);
        $reportService = $this->get('app.financial_report_service');

        $form = $reportService->getBudgetForm($report);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $aryData = $form->getData();
            foreach($aryData as $arySingleBudget){
                $report->addTaskBudget($arySingleBudget[0]);
                //$em->persist($arySingleBudget[0]);
            }
            $em->persist($report);
            $em->flush();
            $this->addFlash('success', 'Your budgets were saved successfully.');
            return $this->redirectToRoute('financial_report_details', ['id'=>$report->getId()]);
        }
        return $this->render('SundialBundle:reports:budgets.html.twig', [
            'form' => $form->createView(),
            'report' => $report
        ]);
        //dump($aryData);exit;
    }

    /**
     * @Route("/view/{id}", name="financial_report_details", requirements={"id":"\d+"})
     */
    public function reportsViewAction(Request $request, $id){
        $service = $this->get('app.financial_report_service');
        $view = $service->getReportView($id);

        if(!$view){
            $this->addFlash('error', 'Sorry, we were unable to find that report.  Please try again');
            $this->redirectToRoute('reports_dashboard');
        }

        $form = $view['budgetForm'];
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            foreach($form->getData() as $array){
                foreach($array as $objBudget){
                    $em->persist($objBudget);
                }
            }
            $em->flush();
            return $this->redirectToRoute('financial_report_details', ['id' => $id]);
        }else{
            $view['budgetForm'] = $view['budgetForm']->createView();
        }

        $form = $view['reportForm'];
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();
            return $this->redirectToRoute('financial_report_details', ['id' => $id]);
        }else{
            $view['reportForm'] = $view['reportForm']->createView();
        }


        if($this->denyAccessUnlessGranted('view', $view['report'])){

        }
        return $this->render('SundialBundle:reports:view.html.twig', [
            'report' => $view
        ]);
    }

}