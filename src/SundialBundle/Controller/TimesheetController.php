<?php

namespace SundialBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SundialBundle\Entity\TogglKey;
use SundialBundle\Entity\TogglProjectSetting;
use SundialBundle\Exception\MultipleTimesheetException;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use SundialBundle\Exception\SubmitWarningException;
use SundialBundle\Exception\SubmitErrorException;

/**
 * Class TimesheetController
 * @package SundialBundle\Controller
 * @Route("/timesheet")
 * @Security("has_role('ROLE_USER')")
 */
class TimesheetController extends Controller
{

    /**
     * @Route("/")
     * @Route("/{day}", name="timesheet", requirements={
     *      "day": "\d{4}-\d{2}-\d{2}"
     * })
     */
    public function timesheetAction(Request $request, $day=null){
        $objSession = $this->get('session');
        if(is_null($day)){
            // we need a day, let's add todays date and redirect
           return $this->redirectToRoute('timesheet', ['day' => date('Y-m-d')]);
        }

        // get the day for this request
        $day = new \DateTime($day);

        // get the timesheet service
        $objTimesheetService = $this->get('app.timesheet_service');

        // get the timesheet data
        try{
            $aryTimesheetData = $objTimesheetService->getTimesheetDayView($day, false);
        }catch(MultipleTimesheetException $e){
            // there are multiple timesheets.  We need to ask the user which timesheet they want
            $this->get('session')->set('timesheet.duplicates', $e->getTimesheets());
            return $this->redirectToRoute('multiple_timesheets');
        }

        // we need a create timesheet form
        if(is_null($aryTimesheetData['Timesheet'])){
            $createTimesheetForm = $objTimesheetService->getCreateTimesheetForm($this->createFormBuilder());
            $createTimesheetForm->handleRequest($request);

            if($createTimesheetForm->isSubmitted()){
                $aryData = $createTimesheetForm->getData();
                $objTimesheetService->createAndRefreshTimesheet($day, $aryData['timeType']);
                return $this->redirectToRoute('timesheet', ['day'=> $day->format('Y-m-d')]);
            }
        }

        // get the timesheet form
        $form = $objTimesheetService->getTimeEntryForm($day);
        $form->handleRequest($request);

        // handle submit
        if ($form->isSubmitted()) {
            if($form->isValid()){
                if($objSession->has('toggltime')){
                    // since imported toggltime is done through ajax,
                    // we need to use session to store the data
                    // but here don't want it as it will interfere with the
                    // submitAndRefreshTimesheet function
                    $objSession->remove('toggltime');
                }

                // we need to merge the arrays from the new and existing records
                $aryPassToOA = [];
                foreach($form->all() as $key => $objForm){
                    $aryData = $objForm->getData();
                    if(is_array($aryData)){
                        if($key == 'new' && count($aryData) > 0){
                            $aryPassToOA = array_merge($aryData, $aryPassToOA);
                        }elseif($key != 'new'){
                            $aryPassToOA = array_merge($aryData, $aryPassToOA);
                        }
                    }
                }
                // because this is submitted, we want to pull open air data again
                // this is also assuming that we already have timesheet data
                if(count($_POST)>0){
                    $this->get('monolog.logger.oa_xml')->debug(json_encode($_POST), ['_POST']);
                    $this->get('monolog.logger.oa_xml')->debug(json_encode($aryPassToOA), ['aryPassToOA']);
                }
                $objTimesheetService->submitAndRefreshTimesheet($aryPassToOA, $day);
                return $this->redirectToRoute('timesheet', ['day'=> $day->format('Y-m-d')]);
            }else{
                $this->get('monolog.logger.oa_xml')->debug('invalid form submission: '.$form->getErrors());
            }
        }

        // we need to handle the imported submit
        // this is set in the toggl service with
        // getTimeRecords called by ajax
        if($objSession->has('toggltime')){
            // there is a toggle session, let's get the form
            $objTogglService = $this->get('app.toggl_service');
            $importForm = $objTogglService->getForm($this->createFormBuilder(), $objSession->get('toggltime'));

            // is the form submitted?
            $importForm->handleRequest($request);

            // it is, let's process it
            if ($importForm->isSubmitted() && $importForm->isValid()) {

                // data is sitting right on the edge of the timeToImport collection
                $aryPassToOA = $importForm->get('timeToImport')->getData();

                // because this is submitted, we want to push AND pull open air data
                $objTimesheetService->submitAndRefreshTimesheet($aryPassToOA, $day);

                // remove the time as we no longer need it
                $objSession->remove('toggltime');

                $this->addFlash('success', "Your toggl time was imported successfully");
                return $this->redirectToRoute('timesheet', ['day'=> $day->format('Y-m-d')]);
            }
        }

        // set some flash messages as appropriate
        if(is_array($aryTimesheetData)){
            if($aryTimesheetData['RejectedTimesheets'] > 0){
                $this->addFlash('error', "You have ".$aryTimesheetData['RejectedTimesheets']." rejected timesheet(s) that need to be handled.");
            }
        }else{
            $this->addFlash('error', "There was an error fetching timesheet information from Open Air");
        }

        // timesheet reminder for the end of the month
        if(date('j') == 1 && count($aryTimesheetData['SubmittedTimesheets']) == 0){
            $this->addFlash('info', "Don't forget to submit your timesheet for the previous month!");
        }

        // editing time on a day that was submitted
        if(!is_null($aryTimesheetData['Timesheet']) && $aryTimesheetData['Tasks']['daybreakdown'][$day->format("D")]['submitted']){
            $this->addFlash('success', "This day belongs to a timesheet that already has been submitted. Please consult Open Air to change time associated with this day.");
        }

        if($this->get('session')->has('timesheet.default')){
            $this->addFlash('info', "You have 2 timesheets open. To avoid confusion, we're only showing time in sundial associated with timesheet #".$this->get('session')->get('timesheet.default').".  We also have hidden your 'Time Worked Per Client :: Project' and 'Utilization' graphs as these will be inaccurate.  To fix this problem, navigate to Open Air and submit / close one of your open timesheets.  Then return to Sundial and Import from Open Air");
            $bHideGraphs = true;
        }

        // render the page
        return $this->render('SundialBundle:timesheet:day.html.twig', [
            'timesheet' => $aryTimesheetData,
            'form' => $form->createView(),
            'day' => $day,
            'createTimesheetForm' => (isset($createTimesheetForm)?$createTimesheetForm->createView():false),
            'submitted' => $aryTimesheetData['Tasks']['daybreakdown'][$day->format("D")]['submitted'],
            'hideGraphs' => isset($bHideGraphs)
        ]);
    }

    /**
     * @Route("/selection", name="multiple_timesheets")
     */
    public function multipleTimesheetsAction(Request $request){
        $objSession = $this->get('session');
        if($objSession->has('timesheet.duplicates')){
            $aryTimesheets = $objSession->get('timesheet.duplicates');
            $aryChoices = [];
            foreach($aryTimesheets as $objTimesheet){
                $aryChoices[$objTimesheet->id] = 'Timesheet Created '.date('D, M. jS', $objTimesheet->created).' (ID# '.$objTimesheet->id.")";
            }
            $form = $this->createFormBuilder();
            $form->add('timesheet', ChoiceType::class, [
                'expanded' => true,
                'choices' => $aryChoices
            ]);
            $form->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn-primary']
            ]);
            $form = $form->getForm();
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $aryData = $form->getData();
                $objSession->set('timesheet.default', $aryData['timesheet']);
                $objSession->remove('timesheet.duplicates');
                return $this->redirectToRoute('timesheet');
            }
            return $this->render('SundialBundle:timesheet:multipleTimesheets.html.twig', [
                'form' => $form->createView()
            ]);
        }else{
            return $this->redirectToRoute('timesheet');
        }

    }

    /**
     * @Route("/togglsettings", name="toggl_settings")
     */
    public function togglSettingsAction(Request $request){
        $objTogglService = $this->get('app.toggl_service');

        $form = $objTogglService->getSettingsForm($this->createFormBuilder());

        if($form === false){
            return $this->redirectToRoute('toggl_auth');
        }
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $aryFormData = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $objSettingsRepo = $this->getDoctrine()->getRepository('SundialBundle:TogglProjectSetting');

            foreach($aryFormData['existing'] as $updatingRecord){
                $obj = $objSettingsRepo->find($updatingRecord['id']);
                if($obj){
                    if(!empty($updatingRecord['project'])){
                        $obj->setTimetype($updatingRecord['timetype']);
                        $obj->setCustomer($updatingRecord['project']->getCustomer());
                        $obj->setProject($updatingRecord['project']);
                        $obj->setTask($updatingRecord['task']);
                        $em->persist($obj);
                    }
                }else{
                    $this->addFlash('warn', 'There was an issue saving one or more of your toggl settings.  The client::project field wasn\'t selected');
                }
            }
            foreach($aryFormData['new'] as $creatingRecord){
                if(!is_null($creatingRecord['timetype']) && !is_null($creatingRecord['project']) && !is_null($creatingRecord['task'])){
                    $objTogglService->updateCreateSetting(
                        $creatingRecord['timetype'],
                        $creatingRecord['project'],
                        $creatingRecord['task'],
                        $this->getUser(),
                        $creatingRecord['togglProjectId']
                    );
                }
            }
            $em->flush();
            $this->addFlash('success', "Settings saved. Go buy yourself something pretty.");
            return $this->redirectToRoute('timesheet');
            //$form = $objTogglService->getSettingsForm($this->createFormBuilder());
        }


        return $this->render('SundialBundle:timesheet:togglsettings.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/togglauth", name="toggl_auth")
     */
    public function togglAuthAction(Request $request){
        $form = $this->createFormBuilder();
        $form->add('togglUser', EmailType::class, [
            'label' => false,
            'attr' => ['placeholder' => 'Toggl Username']
        ]);
        $form->add('togglPassword', PasswordType::class, [
            'label' => false,
            'attr' => ['placeholder' => 'Toggl Password', 'style' => 'margin-top: 15px;']
        ]);
        $form->add('login', SubmitType::class, [
            'attr' => ['class' => 'btn-primary btn-block', 'style' => 'margin-top: 15px;']
        ]);
        $form = $form->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $aryLoginData = $form->getData();
            $objTogglService = $this->get('app.toggl_service');
            $aryResult = $objTogglService->attemptAuth($aryLoginData['togglUser'], $aryLoginData['togglPassword']);
            $me = $this->getDoctrine()->getManager();
            if($aryResult['info']['http_code'] == 200){
                $aryUserData = json_decode($aryResult['result'], true);
                $obj = new TogglKey();
                $obj->setUser($this->getUser());
                $obj->setToken($aryUserData['data']['api_token']);
                $obj->setWorkspaceId($aryUserData['data']['workspaces'][0]['id']);
                $me->persist($obj);
                $me->flush();
                $this->addFlash('success', 'You have been successfully authenticated.  Please try your toggl action again.');
                return $this->redirectToRoute("timesheet");
            }else{
                $this->addFlash('error', "There was an error with your Toggl authentication.  Please try again.");
            }
        }
        return $this->render('SundialBundle:timesheet:togglauth.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/submitTimesheet/{timesheetId}", name="timesheet_submit", requirements={
     *      "timesheetId": "\d+"
     * })
     */
    public function submitTimesheetAction($timesheetId, Request $request){
        $objTimesheetService = $this->get('app.timesheet_service');
        $form = $this->createFormBuilder();

        $form->add('ack', CheckboxType::class, ['required' => true, 'label' => 'I acknowledge this warning.']);
        $form->add('submit', SubmitType::class, ['attr' => ['class' => 'btn btn-primary']]);
        $form = $form->getForm();
        $form->handleRequest($request);

        try{
            if($form->isSubmitted() && $form->isValid()){
                $result = $objTimesheetService->submitTimesheet($timesheetId, true);
            }else{
                $result = $objTimesheetService->submitTimesheet($timesheetId);
            }
        }catch(SubmitWarningException $warning){
            return $this->render('SundialBundle:timesheet:submitissue.html.twig', [
                'warning' => $warning,
                'form' => $form->createView()
            ]);
        }catch(SubmitErrorException $error){
            $this->addFlash('error', 'There was an error when submitting your timesheet: '.$error->getMessage().' ('.$error->getCode().')');
            return $this->redirectToRoute('timesheet');
        }

        if($result){
            $this->addFlash('success', 'Your timesheet was submitted Successfully');
            $objTimesheetService->clearLocalCache();
        }else{
            $this->addFlash('error', 'There was an unidentified error when submitting your timesheet.  Please navigate to Open Air.');
        }
        return $this->redirectToRoute('timesheet');
    }

    /**
     * @Route("/clearCache", name="timesheet_clear_cache")
     */
    public function clearLocalCacheAction(Request $request){
        $strReferrer = $request->headers->get('referer');
        $objTimesheetService = $this->get('app.timesheet_service');
        $objTimesheetService->clearLocalCache();
        return $this->redirect($strReferrer);
    }

}
