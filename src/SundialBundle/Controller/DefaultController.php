<?php

namespace SundialBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('timesheet');
        //return $this->render('SundialBundle:default:index.html.twig');
    }

    /**
     * @Route("/secure", name="secure")
     * @Security("has_role('ROLE_USER')")
     */
    public function secureAction(){
        return new Response('hit');
    }
}
