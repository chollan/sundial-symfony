<?php

namespace SundialBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SundialBundle\Entity\User;
use SundialBundle\Service\UserEventRecorder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(){
        if($this->getUser() instanceof User){
            return $this->redirectToRoute('timesheet', ['day' => date('Y-m-d')]);
        }
        $authenticationUtils = $this->get('security.authentication_utils');

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('SundialBundle:security:login.html.twig', array(
            'last_username' => $lastUsername
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(){
        // anything in here doesn't get called
    }
}