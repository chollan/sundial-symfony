<?php

namespace SundialBundle\Controller;


use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SundialBundle\Entity\UserLastTask;
use SundialBundle\Service\UserEventRecorder;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RestController
 * @package SundialBundle\Controller
 * @Route("/rest", defaults={"_format":"json"})
 */
class RestController extends FOSRestController
{

    /**
     * @Route("/tasks/{projectid}", name="rest_get_tasks", requirements={
     *    "projectid" : "\d+"
     * })
     * @Method({"GET"})
     * @Security("has_role('ROLE_USER')")
     */
    public function getTasksAction($projectid){
        $objProjectTaskRepo = $this->getDoctrine()->getRepository('SundialBundle:ProjectTask');
        $aryData = $objProjectTaskRepo->getTasksForProject($projectid);
        if(count($aryData) > 0){
            $objLastTask = $this->getDoctrine()->getRepository('SundialBundle:UserLastTask')->findOneBy(['project'=>$aryData[0]->getProject(), 'user' => $this->getUser()]);
        }

        $aryReturn = [];
        // we need to build an array becaue of how json orders data
        // we will loose the 1., 2. indicies
        foreach($aryData as $id => $task){
            $aryTmp = ['id' => $id, 'task' => $task, 'selected' => false];
            if(isset($objLastTask) && $objLastTask->getProjectTask() == $task){
                $aryTmp['selected'] = true;
            }
            $aryReturn[] = $aryTmp;
        }
        $view = new View($aryReturn, 200);
        return $this->handleView($view);
    }

    /**
     * @Route("/lastTask", name="rest_set_last_task")
     * @Method({"POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function setLastTaskAction(Request $request){
        $intProjectTaskId = $request->get('task');
        if(is_null($intProjectTaskId)){
            return $this->handleView(new View(null, 400));
        }

        $objProjectTask = $this->getDoctrine()->getRepository('SundialBundle:ProjectTask')->find($intProjectTaskId);
        if($objProjectTask){
            $objLastTask = $this->getDoctrine()->getRepository('SundialBundle:UserLastTask')->findOneBy([
                'user' => $this->getUser(),
                'project' => $objProjectTask->getProject()
            ]);

            $em = $this->getDoctrine()->getManager();
            if(is_null($objLastTask)){
                $objLastTask = new UserLastTask($this->getUser(), $objProjectTask->getProject(), $objProjectTask);
            }else{
                $objLastTask->setProjectTask($objProjectTask);
            }
            $em->persist($objLastTask);
            $em->flush();
            return $this->handleView(new View(null, 200));
        }else{
            return $this->handleView(new View(null, 404));
        }
    }

    /**
     * @Route("/fetchTogglTime/{day}", name="rest_get_toggl_time", requirements={
     *    "day": "\d{4}-\d{2}-\d{2}"
     * })
     * @Method({"GET"})
     * @Security("has_role('ROLE_USER')")
     */
    public function fetchTogglTimeAction($day){
        $objTogglService = $this->get('app.toggl_service');

        // we're setting the day here so that we can use it in the task type below
        $objTimesheetService = $this->get('app.timesheet_service');
        $objTimesheetService->setDay(new \DateTime($day));

        $aryRequestResult = $objTogglService->getTimeRecords(new \DateTime($day));
        if($aryRequestResult === false){
            $view = new View(null, 401);
            return $this->handleView($view);
        }

        if($aryRequestResult['info']['http_code'] == 200){
            $aryTogglTime = json_decode($aryRequestResult['result'], true);
            if(count($aryTogglTime) == 0){
                $view = new View(null, 204);
                return $this->handleView($view);
            }

            // we only want to return time entries that are not running
            // and entries that haven't already be imported
            $aryImportedRepo = $this->getDoctrine()->getRepository('SundialBundle:TogglImportedRecord');
            $aryReturn = [];
            foreach($aryTogglTime as $aryTime){
                $objExisting = $aryImportedRepo->findOneBy([
                    'user' => $this->getUser(),
                    'togglId' => $aryTime['id']
                ]);
                if($aryTime['duration'] >= 0 && is_null($objExisting)){
                    $aryReturn[] = $aryTime;
                }
            }
            if(count($aryReturn) > 0){
                $this->container->get('app.event_recorder')->RecordEvent($this->getUser(), UserEventRecorder::IMPORT);
                $this->get('session')->set('toggltime', $aryReturn);
                $form = $objTogglService->getForm($this->createFormBuilder(), $aryReturn);

                $view = new View(
                    $this->render('SundialBundle:api:togglimport.html.twig', ['form' => $form->createView()])->getContent(),
                    200
                );
                return $this->handleView($view);
            }else{
                $view = new View(null, 204);
                return $this->handleView($view);
            }

        }
    }

    /**
     * @Route("/projects/{customerid}", requirements={
     *    "customerid": "\d+"
     * })
     */
    public function getCustomerProjects($customerid){
        $em = $this->getDoctrine()->getManager();
        $projectRepo = $em->getRepository('SundialBundle:Project');
        $aryProjects = $projectRepo->findBy([
            'customer' => $customerid,
            'active' => true
        ]);

        if(count($aryProjects) > 0){
            $aryReturn = [];
            foreach($aryProjects as $objProject){
                $aryReturn[] = ['id' => $objProject->getId(), 'name' => $objProject->getName()];
            }
            usort($aryReturn, function($a, $b) {
                return strcmp($a["name"], $b["name"]);
            });
            return $this->handleView(new View($aryReturn));
        }

        return $this->handleView(new View(null, 204));
    }

    /**
     * @Route("/report/run/{reportID}", name="rest_run_report", requirements={"reportid":"\d"})
     * @Method({"GET"})
     */
    public function runReport($reportID){
        $objReport = $this->getDoctrine()->getRepository('SundialBundle:FinancialReport')->find($reportID);
        $this->denyAccessUnlessGranted('run', $objReport);
        $objReportService = $this->get('app.financial_report_service');
        $response = $objReportService->runReport($objReport);
        if($response instanceof \DateTime){
            return $this->handleView(new View($response->format('Y-m-d H:i:s'), 200));
        }else{
            return $this->handleView(new View(null, 500));
        }
    }
}