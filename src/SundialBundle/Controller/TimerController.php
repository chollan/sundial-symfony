<?php

namespace SundialBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TimerController
 * @package SundialBundle\Controller
 * @Route("/timer")
 */
class TimerController extends Controller
{

    /**
     * @Route("/", name="timer")
     * @Security("has_role('ROLE_USER')")
     */
    public function timerAction(){
        return $this->render('SundialBundle:timer:home.html.twig', []);
    }

}