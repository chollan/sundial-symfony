<?php

namespace SundialBundle\DependencyInjection;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\DependencyInjection\Extension\Extension;

class SundialExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @param array $configs An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $env = $container->getParameter("kernel.environment");
        if(file_exists(__DIR__ . '/../Resources/config/'.$env.'/sundial.yml')){
            $configs = array_merge($configs,Yaml::parse(__DIR__ . '/../Resources/config/'.$env.'/sundial.yml'));
            $configuration = new Configuration();
            $config = $this->processConfiguration($configuration, $configs);
            foreach($config as $key => $val){
                $container->setParameter('sundial.'.$key, $val);
            }
        }
    }
}