<?php

namespace SundialBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sundial');

        $rootNode
            ->children()
                ->scalarNode('oa_namespace')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('oa_key')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('oa_company')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('oa_client')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('oa_app_version')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('oa_server')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('default_user')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('default_pass')->isRequired()->cannotBeEmpty()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}