<?php

namespace SundialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SundialBundle\Annotations\OpenAirField;

/**
 * TimeType
 *
 * @ORM\Table(name="time_type")
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\TimeTypeRepository")
 */
class TimeType
{

    const BILLABLE = 1;
    const NON_BILLABLE = 2;
    const NON_BILLABLE_TRAVEL= 3;
    const NON_BILLABLE_TRAINING = 7;
    const NON_BILLABLE_SALES = 8;
    const NON_BILLABLE_INNOVATION = 9;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @OpenAirField("id")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     * @OpenAirField("name")
     */
    private $type;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     * @OpenAirField("active")
     */
    private $active;

    /**
     * @var datetime
     *
     * @ORM\Column(name="updated", type="datetime")
     * @OpenAirField("updated")
     */
    private $updated;

    function __toString()
    {
        return $this->type;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return TimeType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return TimeType
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        if($updated instanceof \DateTime){
            $this->updated = $updated;
        }else{
            $this->updated = new \DateTime(date('Y-m-d H:i:s', $updated));
        }

    }
}