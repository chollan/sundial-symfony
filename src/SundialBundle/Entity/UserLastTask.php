<?php

namespace SundialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use SundialBundle\Annotations\OpenAirField;

/**
 * UserLastTask
 *
 * @ORM\Table(name="user_last_task")
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\UserLastTaskRepository")
 */
class UserLastTask
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $project;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="ProjectTask")
     * @ORM\JoinColumn(name="projecttask_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $task;

    function __construct(User $user = null, Project $project = null, ProjectTask $task = null)
    {
        if(!is_null($user))
            $this->user = $user;
        if(!is_null($project))
            $this->project = $project;
        if(!is_null($task))
            $this->task = $task;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return UserLastTask
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set project
     *
     * @param integer $project
     * @return UserLastTask
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set task
     *
     * @param integer $task
     * @return UserLastTask
     */
    public function setProjectTask($task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return integer 
     */
    public function getProjectTask()
    {
        return $this->task;
    }
}
