<?php

namespace SundialBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * TogglImportedRecord
 *
 * @ORM\Table(name="toggl_imported_record")
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\TogglImportedRecordRepository")
 */
class TogglImportedRecord
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="togglId", type="integer")
     */
    private $togglId;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    function __construct($intTogglID = null, User $user = null)
    {
        if(!is_null($intTogglID)){
            $this->togglId = $intTogglID;
        }
        if(!is_null($user)){
            $this->user = $user;
        }
        $this->date = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set togglId
     *
     * @param integer $togglId
     * @return TogglImportedRecord
     */
    public function setTogglId($togglId)
    {
        $this->togglId = $togglId;

        return $this;
    }

    /**
     * Get togglId
     *
     * @return integer 
     */
    public function getTogglId()
    {
        return $this->togglId;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return TogglImportedRecord
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return TogglImportedRecord
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
}
