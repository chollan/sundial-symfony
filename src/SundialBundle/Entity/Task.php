<?php

namespace SundialBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Doctrine\ORM\Mapping as ORM;

/**
 * Task
 *
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\TaskRepository")
 */
class Task
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="FinancialReport")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $report;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="TimeType")
     * @ORM\JoinColumn(name="timetype_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $timeType;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="ProjectTask")
     * @ORM\JoinColumn(name="projecttask_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $projectTask;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="decimalHours", type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     */
    private $decimalHours;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    public function setId($id){
        $this->id = $id;

        return $this;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set report
     *
     * @param integer $report
     * @return TimeRecord
     */
    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return integer 
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return TimeRecord
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set timeType
     *
     * @param integer $timeType
     * @return TimeRecord
     */
    public function setTimeType($timeType)
    {
        $this->timeType = $timeType;

        return $this;
    }

    /**
     * Get timeType
     *
     * @return integer 
     */
    public function getTimeType()
    {
        return $this->timeType;
    }

    /**
     * Set projectTask
     *
     * @param integer $projectTask
     * @return TimeRecord
     */
    public function setProjectTask($projectTask)
    {
        $this->projectTask = $projectTask;

        return $this;
    }

    /**
     * Get projectTask
     *
     * @return integer 
     */
    public function getProjectTask()
    {
        return $this->projectTask;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TimeRecord
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set decimalHours
     *
     * @param string $decimalHours
     * @return TimeRecord
     */
    public function setDecimalHours($decimalHours)
    {
        $this->decimalHours = $decimalHours;

        return $this;
    }

    /**
     * Get decimalHours
     *
     * @return string 
     */
    public function getDecimalHours()
    {
        return $this->decimalHours;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return TimeRecord
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context){
        if ((($this->decimalHours*100)%(25)) !== 0) {
            $context->buildViolation('Please enter time in increments of 15 minutes.')
                ->atPath('decimalHours')
                ->addViolation();
        }
    }
}
