<?php

namespace SundialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use SundialBundle\Annotations\OpenAirField;

/**
 * ProjectTask
 *
 * @ORM\Table(name="project_task")
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\ProjectTaskRepository")
 */
class ProjectTask
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @OpenAirField("id")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     * @OpenAirField("customerid", target="Customer")
     */
    private $customer;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     * @OpenAirField("projectid", target="Project")
     */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @OpenAirField("name")
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     * @OpenAirField("is_a_phase")
     */
    private $isPhase;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="ProjectTask")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     * @OpenAirField("parentid", target="ProjectTask")
     */
    private $parent;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @OpenAirField("seq")
     */
    private $sequence;

    /**
     * @var int
     *
     * @ORM\Column(type="decimal", precision=10, scale=1)
     * @OpenAirField("id_number")
     */
    private $idNumber;

    /**
     * @ORM\Column(type="datetime")
     * @OpenAirField("updated")
     */
    private $updated;

    /**
     * @ORM\Column(type="boolean")
     * @OpenAirField("closed")
     */
    private $closed;

    public function __toString(){
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set customer
     *
     * @param integer $customer
     * @return ProjectTask
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return integer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set project
     *
     * @param integer $project
     * @return ProjectTask
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProjectTask
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isPhase
     *
     * @param boolean $isPhase
     * @return ProjectTask
     */
    public function setIsPhase($isPhase)
    {
        $this->isPhase = $isPhase;

        return $this;
    }

    /**
     * Get isPhase
     *
     * @return boolean 
     */
    public function getIsPhase()
    {
        return $this->isPhase;
    }

    public function isPhase(){
        return $this->isPhase;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     * @return ProjectTask
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return integer 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return ProjectTask
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set idNumber
     *
     * @param integer $idNumber
     * @return ProjectTask
     */
    public function setIdNumber($idNumber)
    {
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * Get idNumber
     *
     * @return integer 
     */
    public function getIdNumber()
    {
        return $this->idNumber;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        if($updated instanceof \DateTime){
            $this->updated = $updated;
        }else{
            $this->updated = new \DateTime(date("Y-m-d H:i:s", $updated));
        }

    }

    /**
     * @return mixed
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * @param mixed $closed
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;
    }

}
