<?php

namespace SundialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use SundialBundle\Annotations\OpenAirField;
use VMelnik\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @OpenAirField("id")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=100)
     * @Assert\NotBlank()
     * @OpenAirField("nickname")
     */
    private $nickname;

    /**
     * @var string
     *
     * @ORM\Column(name="picklistLabel", type="string", length=100)
     * @Assert\NotBlank()
     * @OpenAirField("picklist_label")
     */
    private $picklistLabel;

    /**
     * @ORM\Column(type="integer")
     * @OpenAirField("job_codeid")
     */
    private $jobcode;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $roles;

    /**
     * @ORM\Column(type="string")
     * @Encrypted
     */
    private $password;

    public function __construct()
    {
        $this->resetRoles();
    }

    public function setId($id){
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     * @return User
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string 
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set picklistLabel
     *
     * @param string $picklistLabel
     * @return User
     */
    public function setPicklistLabel($picklistLabel)
    {
        $this->picklistLabel = $picklistLabel;

        return $this;
    }

    /**
     * Get picklistLabel
     *
     * @return string 
     */
    public function getPicklistLabel()
    {
        return $this->picklistLabel;
    }

    public function __toString(){
        return (string)$this->picklistLabel;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles(array $roles){
        $this->roles = $roles;
    }

    public function addRole($role){
        if(!in_array($role, $this->roles)){
            $this->roles[] = strtoupper($role);
        }
    }

    public function resetRoles(){
        $this->roles = ['ROLE_USER'];
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->nickname;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        return null;
    }

    /**
     * @return mixed
     */
    public function getJobcode()
    {
        return $this->jobcode;
    }

    /**
     * @param mixed $jobcode
     */
    public function setJobcode($jobcode)
    {
        $this->jobcode = $jobcode;
    }

}
