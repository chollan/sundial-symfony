<?php

namespace SundialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Budget
 *
 * @ORM\Table(name="budget")
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\BudgetRepository")
 */
class Budget
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="FinancialReport", inversedBy="taskBudgets")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $report;

    /**
     * @var int
     *
     * @ORM\OneToOne(targetEntity="ProjectTask")
     * @ORM\JoinColumn(name="projecttask_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $task;

    /**
     * @var int
     *
     * @ORM\Column(name="hours", type="decimal", precision=10, scale=2)
     * @Assert\GreaterThan(value=0, message="This number must be positive")
     */
    private $hours;

    /**
     * @var string
     *
     * @ORM\Column(name="dollars", type="decimal", precision=10, scale=2)
     * @Assert\GreaterThan(value=0, message="This number must be positive")
     */
    private $dollars;

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set report
     *
     * @param integer $report
     * @return Budget
     */
    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return integer 
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set task
     *
     * @param integer $task
     * @return Budget
     */
    public function setTask($task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return integer 
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set hours
     *
     * @param integer $hours
     * @return Budget
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return integer 
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set dollars
     *
     * @param string $dollars
     * @return Budget
     */
    public function setDollars($dollars)
    {
        $this->dollars = $dollars;

        return $this;
    }

    /**
     * Get dollars
     *
     * @return string 
     */
    public function getDollars()
    {
        return $this->dollars;
    }

    public function __toString(){
        return (string)$this->id;
    }
}
