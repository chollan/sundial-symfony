<?php

namespace SundialBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * TogglProjectSetting
 *
 * @ORM\Table(name="toggl_project_setting")
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\TogglProjectSettingRepository")
 */
class TogglProjectSetting
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="togglProjectId", type="integer")
     */
    private $togglProjectId;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="TimeType")
     * @ORM\JoinColumn(name="timetype_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $timetype;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $customer;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $project;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="ProjectTask")
     * @ORM\JoinColumn(name="projecttask_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $task;

    function __toString()
    {
        return strval($this->id);
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set togglProjectId
     *
     * @param integer $togglProjectId
     * @return TogglProjectSetting
     */
    public function setTogglProjectId($togglProjectId)
    {
        $this->togglProjectId = $togglProjectId;

        return $this;
    }

    /**
     * Get togglProjectId
     *
     * @return integer 
     */
    public function getTogglProjectId()
    {
        return $this->togglProjectId;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return TogglProjectSetting
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set timetype
     *
     * @param integer $timetype
     * @return TogglProjectSetting
     */
    public function setTimetype($timetype)
    {
        $this->timetype = $timetype;

        return $this;
    }

    /**
     * Get timetype
     *
     * @return integer 
     */
    public function getTimetype()
    {
        return $this->timetype;
    }

    /**
     * Set customer
     *
     * @param integer $customer
     * @return TogglProjectSetting
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return integer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set project
     *
     * @param integer $project
     * @return TogglProjectSetting
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return int
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param int $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }


}
