<?php

namespace SundialBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FinancialReport
 *
 * @ORM\Table(name="financial_report")
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\FinancialReportRepository")
 */
class FinancialReport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull()
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\OneToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lastRunStatus;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastRun;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\GreaterThan(value=0, message="This number must be positive")
     */
    private $budgetHigh;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\GreaterThan(value=0, message="This number must be positive")
     */
    private $budgetLow;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\GreaterThan(value=0, message="This number must be positive")
     */
    private $averageRate;

    /**
     * @var string
     *
     * @ORM\Column(type="integer")
     * @Assert\GreaterThan(value=0, message="This number must be positive")
     */
    private $hoursToFinish;

    /**
     * @ORM\OneToMany(targetEntity="Budget", mappedBy="report", cascade={"persist"})
     */
    private $taskBudgets;

    /**
     * @ORM\OneToMany(targetEntity="Rate", mappedBy="report", cascade={"persist"})
     */
    private $rates;

    function __construct()
    {
        $this->taskBudgets = new ArrayCollection();
        $this->rates = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FinancialReport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return FinancialReport
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set project
     *
     * @param integer $project
     * @return FinancialReport
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return FinancialReport
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return FinancialReport
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set lastRunStatus
     *
     * @param integer $lastRunStatus
     * @return FinancialReport
     */
    public function setLastRunStatus($lastRunStatus)
    {
        $this->lastRunStatus = $lastRunStatus;

        return $this;
    }

    /**
     * Get lastRunStatus
     *
     * @return integer 
     */
    public function getLastRunStatus()
    {
        return $this->lastRunStatus;
    }

    /**
     * Set budgetHigh
     *
     * @param string $budgetHigh
     * @return FinancialReport
     */
    public function setBudgetHigh($budgetHigh)
    {
        $this->budgetHigh = $budgetHigh;

        return $this;
    }

    /**
     * Get budgetHigh
     *
     * @return string 
     */
    public function getBudgetHigh()
    {
        return $this->budgetHigh;
    }

    /**
     * Set budgetLow
     *
     * @param string $budgetLow
     * @return FinancialReport
     */
    public function setBudgetLow($budgetLow)
    {
        $this->budgetLow = $budgetLow;

        return $this;
    }

    /**
     * Get budgetLow
     *
     * @return string 
     */
    public function getBudgetLow()
    {
        return $this->budgetLow;
    }

    /**
     * Set averageRate
     *
     * @param string $averageRate
     * @return FinancialReport
     */
    public function setAverageRate($averageRate)
    {
        $this->averageRate = $averageRate;

        return $this;
    }

    /**
     * Get averageRate
     *
     * @return string 
     */
    public function getAverageRate()
    {
        return $this->averageRate;
    }

    /**
     * Set hoursToFinish
     *
     * @param string $hoursToFinish
     * @return FinancialReport
     */
    public function setHoursToFinish($hoursToFinish)
    {
        $this->hoursToFinish = $hoursToFinish;

        return $this;
    }

    /**
     * Get hoursToFinish
     *
     * @return string 
     */
    public function getHoursToFinish()
    {
        return $this->hoursToFinish;
    }

    /**
     * @return mixed
     */
    public function getTaskBudgets()
    {
        return $this->taskBudgets;
    }

    /**
     * @param mixed $taskBudgets
     */
    public function setTaskBudgets($taskBudgets)
    {
        $this->taskBudgets = $taskBudgets;
    }

    public function addTaskBudget(Budget $budget){
        if(!$this->taskBudgets->contains($budget)){
            $this->taskBudgets->add($budget);
        }
        return $this;
    }

    public function removeTaskBudget(Budget $budget){
        if($this->taskBudgets->contains($budget)){
            $this->taskBudgets->remove($budget);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastRun()
    {
        return $this->lastRun;
    }

    /**
     * @param mixed $lastRun
     */
    public function setLastRun($lastRun)
    {
        $this->lastRun = $lastRun;
    }

    /**
     * @return mixed
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * @param mixed $rates
     */
    public function setRates($rates)
    {
        $this->rates = $rates;
    }
}
