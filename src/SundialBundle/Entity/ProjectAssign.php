<?php

namespace SundialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use SundialBundle\Annotations\OpenAirField;


/**
 * ProjectAssign
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\ProjectAssignRepository")
 */
class ProjectAssign
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @OpenAirField("id")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User",cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     * @OpenAirField("user_id", target="User")
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     * @OpenAirField("customer_id", target="Customer")
     */
    private $customer;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     * @OpenAirField("project_id", target="Project")
     */
    private $project;

    /**
     * @ORM\Column(type="datetime")
     * @OpenAirField("updated")
     */
    private $updated;

    function __construct()
    {
    }

    function __toString()
    {
        return $this->user->getPicklistLabel().' -> '.$this->customer->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return ProjectAssign
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getProjectTask()
    {
        return $this->projectTask;
    }

    /**
     * @param int $projectTask
     */
    public function setProjectTask($projectTask)
    {
        $this->projectTask = $projectTask;
    }

    /**
     * @return int
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param int $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param mixed $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        if($updated instanceof \DateTime){
            $this->updated = $updated;
        }else{
            $this->updated = new \DateTime(date("Y-m-d H:i:s", $updated));
        }

    }
}
