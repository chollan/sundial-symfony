<?php

namespace SundialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use SundialBundle\Annotations\OpenAirField;

/**
 * Class Customer
 * @package SundialBundle\Entities
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\CustomerRepository")
 * @ORM\Table()
 */
class Customer
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @OpenAirField("id")
     */
    private $id;

    /**
     * @var
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @OpenAirField("name")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     * @OpenAirField("active")
     */
    private $active;

    /**
     * @ORM\Column(type="datetime")
     * @OpenAirField("updated")
     */
    private $updated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        if($updated instanceof \DateTime)
            $this->updated = $updated;
        else
            $this->updated = new \DateTime(date("Y-m-d H:i:s", $updated));
    }

    public function __toString(){
        return $this->name;
    }


}