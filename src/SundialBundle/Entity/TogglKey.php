<?php

namespace SundialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TogglKey
 *
 * @ORM\Table(name="toggl_key")
 * @ORM\Entity(repositoryClass="SundialBundle\Repository\TogglKeyRepository")
 */
class TogglKey
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $token;

    /**
     * @var int
     *
     * @ORM\Column(name="workspaceId", type="integer")
     * @Assert\NotBlank()
     */
    private $workspaceId;

    function __construct(User $user = null, $token = null, $workspaceId = null)
    {
        if(!is_null($user))
            $this->user = $user;
        if(!is_null($token))
            $this->token = $token;
        if(!is_null($workspaceId))
            $this->workspaceId = $workspaceId;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return TogglKey
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return TogglKey
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set workspaceId
     *
     * @param integer $workspaceId
     * @return TogglKey
     */
    public function setWorkspaceId($workspaceId)
    {
        $this->workspaceId = $workspaceId;

        return $this;
    }

    /**
     * Get workspaceId
     *
     * @return integer 
     */
    public function getWorkspaceId()
    {
        return $this->workspaceId;
    }
}
