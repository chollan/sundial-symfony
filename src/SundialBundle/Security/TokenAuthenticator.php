<?php

namespace SundialBundle\Security;


use OpenAir\Base\Command;
use OpenAir\Commands\Auth;
use OpenAir\Commands\Read;
use OpenAir\Commands\Whoami;
use OpenAir\DataTypes\Login;
use SundialBundle\Entity\User;
use SundialBundle\Service\UserEventRecorder;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator
{

    private $authResponse;
    private $credentials;
    private $container;
    private $userEntity;

    function __construct(Container $container)
    {
        $this->container = $container;
        $this->userEntity = $this->container->get('doctrine')->getRepository('SundialBundle:User');
    }

    /**
     * Returns a response that directs the user to authenticate.
     *
     * This is called when an anonymous request accesses a resource that
     * requires authentication. The job of this method is to return some
     * response that "helps" the user start into the authentication process.
     *
     * Examples:
     *  A) For a form login, you might redirect to the login page
     *      return new RedirectResponse('/login');
     *  B) For an API token authentication system, you return a 401 response
     *      return new Response('Auth header required', 401);
     *
     * @param Request $request The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse('login');
    }

    /**
     * Get the authentication credentials from the request and return them
     * as any type (e.g. an associate array). If you return null, authentication
     * will be skipped.
     *
     * Whatever value you return here will be passed to getUser() and checkCredentials()
     *
     * For example, for a form login, you might:
     *
     *      if ($request->request->has('_username')) {
     *          return array(
     *              'username' => $request->request->get('_username'),
     *              'password' => $request->request->get('_password'),
     *          );
     *      } else {
     *          return;
     *      }
     *
     * Or for an API token that's on a header, you might use:
     *
     *      return array('api_key' => $request->headers->get('X-API-TOKEN'));
     *
     * @param Request $request
     *
     * @return mixed|null
     */
    public function getCredentials(Request $request)
    {
        if ($request->request->has('_username')) {
            $this->credentials = array(
                'username' => $request->request->get('_username'),
                'password' => $request->request->get('_password'),
            );
            return $this->credentials;
        } else {
            return;
        }
    }

    /**
     * Return a UserInterface object based on the credentials.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * You may throw an AuthenticationException if you wish. If you return
     * null, then a UsernameNotFoundException is thrown for you.
     *
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     *
     * @throws AuthenticationException
     *
     * @return UserInterface|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try{
            $user = $userProvider->loadUserByUsername($credentials['username']);
        }catch (\Exception $e){
            $user = new User();
            $user->setNickname($credentials['username']);
        }
        return $user;
    }

    /**
     * Returns true if the credentials are valid.
     *
     * If any value other than true is returned, authentication will
     * fail. You may also throw an AuthenticationException if you wish
     * to cause authentication to fail.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * @param mixed $credentials
     * @param UserInterface $user
     *
     * @return bool
     *
     * @throws AuthenticationException
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        $namespace = $this->container->getParameter('sundial.oa_namespace');
        $key = $this->container->getParameter('sundial.oa_key');
        $url = $this->container->getParameter('sundial.oa_server');
        $objRequest = new \OpenAir\Request($namespace, $key, $url);
        //$objRequest->setDebug(true);

        // we want to auth -- that's the whole point here!
        $company = $this->container->getParameter('sundial.oa_company');
        $objRequest->addCommand(new Auth(new Login($company, $credentials['username'], $credentials['password'])));

        // let's get the user we're logging in as
        $objWhoAmICommand = new Whoami();
        $objRequest->addCommand($objWhoAmICommand);

        // let's get the roles
        $objReadCommand = new Read(['type'=>'Role']);
        $objReadCommand->setReturnValues(['name','id']);
        $objRequest->addCommand($objReadCommand);

        $this->authResponse = $objRequest->execute();
        // check the auth response
        $objAuthResponse = $this->authResponse->getCommandResponse('Auth');
        $intResponseStatus = $objAuthResponse->getResponseStatus();
        if($intResponseStatus == Command::STATUS_SUCCESS){
            // get the whoami data
            $objWhoamiResponse = $this->authResponse->getCommandResponse('Whoami');
            $whoamidata = $objWhoamiResponse->getResponseData();

            // if this is a new user, the user's ID will be null.
            if(is_null($user->getId())){
                // HOWEVER if the user name has changed, but the user is still the same, the user may still exist
                // let's check for the USER ID before we start creating a new user
                // OAAPI-49
                $objFetchedUser = $this->userEntity->findOneById($whoamidata[0]->id);
                if(!is_null($objFetchedUser)){
                    $user = $objFetchedUser;
                    // this user already exists.  we need to update the username
                    $user->setNickname($whoamidata[0]->nickname);
                }else{
                    $user->setId($whoamidata[0]->id);
                    $user->setJobcode($whoamidata[0]->job_codeid);
                }
                // we want to set the picklist label if the user is new or has a username change.
                $user->setPicklistLabel($whoamidata[0]->picklist_label);
            }

            // get the read response to get the roles
            $aryReadResponse = $this->authResponse->getCommandResponse('Read');
            $objReadResponse = $aryReadResponse[0]; //there's only 1 read request
            $aryRoleResponse = $objReadResponse->getResponseData();

            // build a roles array so we can reference it later
            $aryRoles = [];
            foreach($aryRoleResponse as $objRole){
                $strRole = preg_replace('/\([^\)]+\)/i','',$objRole->name);
                $strRole = str_replace(['/', ' '],'_' , trim($strRole));
                $aryRoles[$objRole->id] = strtoupper('ROLE_'.$strRole);
            }

            // add the OA role to the users list of roles
            $user->resetRoles();
            $user->addRole($aryRoles[$whoamidata[0]->role_id]);

            // set the user password
            $user->setPassword($credentials['password']);

            // save the user's password and roles
            $em = $this->container->get('doctrine')->getManager();
            $em->persist($user);
            $em->flush();

            // authentication succeeded
            return true;
        }else{
            // authentication failed
            $this->container->get('session')->getFlashBag()->add('error', Auth::$response[$intResponseStatus]);
            return false;
        }
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 403 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     *
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        //$this->container->get('session')->getFlashBag()->add('error', 'Invalid credentials.');
        return new RedirectResponse('login');
    }

    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If you return null, the current request will continue, and the user
     * will be authenticated. This makes sense, for example, with an API.
     *
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey The provider (i.e. firewall) key
     *
     * @return Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $this->container->get('app.event_recorder')->RecordEvent($token->getUser(), UserEventRecorder::LOGIN);
        $objUserService = $this->container->get('app.user_service');
        $objUserService->fillProjectAssignments($token->getUser());
        $key = '_security.main.target_path';
        if($this->container->get('session')->has($key)){
            //set the url based on the link they were trying to access before being authenticated
            $url = $this->container->get('session')->get($key);

            //remove the session key
            $this->container->get('session')->remove($key);
        }else{
            //if the referrer key was never set, redirect to a default route
            $url = $this->container->get('router')->generate('homepage');
        }
        return new RedirectResponse($url);
    }

    /**
     * Does this method support remember me cookies?
     *
     * Remember me cookie will be set if *all* of the following are met:
     *  A) This method returns true
     *  B) The remember_me key under your firewall is configured
     *  C) The "remember me" functionality is activated. This is usually
     *      done by having a _remember_me checkbox in your form, but
     *      can be configured by the "always_remember_me" and "remember_me_parameter"
     *      parameters under the "remember_me" firewall key
     *
     * @return bool
     */
    public function supportsRememberMe()
    {
        return true;
    }
}
