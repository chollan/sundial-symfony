<?php

namespace SundialBundle\Security;


use SundialBundle\Entity\FinancialReport;
use SundialBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class FinancialReportVoter extends Voter
{

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array('view', 'run', 'edit'))) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof FinancialReport) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        $report = $subject;

        switch ($attribute) {
            case 'view':
                return $this->canView($report, $user);
            case 'edit':
                return $this->canEdit($report, $user);
            case 'run':
                return $this->canRun($report, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView($report, $user){
        return true;
    }

    private function canEdit($report, $user){
        if($report->getUser()->getId() == $user->getId()){
            return true;
        }
        return false;
    }

    private function canRun($report, $user){
        if($report->getUser()->getId() == $user->getId()){
            return true;
        }
        return false;
    }
}